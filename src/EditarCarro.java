import java.awt.*;
import javax.swing.*;
import javax.swing.border.*;
import javax.swing.text.DefaultFormatter;

import java.sql.*;
import java.util.Calendar;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;

public class EditarCarro extends JFrame {
	
	public static final String JDBC_DRIVER = "org.sqlite.JDBC";
	public static final String DB_URL = "jdbc:sqlite:projeto.db";
	public Connection conn = null;
	public Statement stmt = null;
	public ResultSet rs;
	
	private JPanel contentPane;
	private JLabel lbl_matricula;
	private JTextField txt_marca;
	private JTextField txt_modelo;
	private JTextField txt_cor;
	private JFormattedTextField txt_ano_aquisicao;
	private JSpinner txt_preco_aquisicao;
	private JFormattedTextField txt_ano_venda;
	private JSpinner txt_valor_venda;
	private JSpinner txt_cilindrada;
	private JComboBox<String> txt_combustivel;
	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					EditarCarro frame = new EditarCarro();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public EditarCarro() throws Exception {
		Class.forName(JDBC_DRIVER);
		conn = DriverManager.getConnection(DB_URL);
		stmt = conn.createStatement();
		
		setTitle("Editar Carro");
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 327, 461);
		setIconImage(new ImageIcon("editar.png").getImage());
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		JLabel lblMatricula = new JLabel("Matricula : #");
		lblMatricula.setForeground(Color.BLACK);
		lblMatricula.setFont(new Font("Tahoma", Font.PLAIN, 14));
		lblMatricula.setBounds(10, 11, 83, 17);
		contentPane.add(lblMatricula);
		
		lbl_matricula = new JLabel("");
		lbl_matricula.setForeground(Color.BLACK);
		lbl_matricula.setFont(new Font("Tahoma", Font.PLAIN, 14));
		lbl_matricula.setBounds(95, 11, 83, 17);
		contentPane.add(lbl_matricula);
		
		JLabel lblMarca = new JLabel("<html>Marca : <font color='red'>*</font></html>");
		lblMarca.setFont(new Font("Tahoma", Font.PLAIN, 14));
		lblMarca.setBounds(10, 60, 75, 14);
		contentPane.add(lblMarca);
		
		JLabel lblModelo = new JLabel("<html>Modelo : <font color='red'>*</font></html>");
		lblModelo.setFont(new Font("Tahoma", Font.PLAIN, 14));
		lblModelo.setBounds(10, 91, 75, 14);
		contentPane.add(lblModelo);
		
		JLabel lblCor = new JLabel("<html>Cor : <font color='red'>*</font></html>");
		lblCor.setFont(new Font("Tahoma", Font.PLAIN, 14));
		lblCor.setBounds(11, 122, 62, 14);
		contentPane.add(lblCor);
		
		JLabel lblCilindrada = new JLabel("<html>Cilindrada : <font color='red'>*</font></html>");
		lblCilindrada.setFont(new Font("Tahoma", Font.PLAIN, 14));
		lblCilindrada.setBounds(10, 155, 84, 14);
		contentPane.add(lblCilindrada);
		
		JLabel lblCombustivel = new JLabel("<html>Combustivel : <font color='red'>*</font></html>");
		lblCombustivel.setFont(new Font("Tahoma", Font.PLAIN, 14));
		lblCombustivel.setBounds(10, 187, 99, 14);
		contentPane.add(lblCombustivel);
		
		JLabel lblAnoDeAquisicao = new JLabel("<html>Ano de Aquisi\u00E7\u00E3o : <font color='red'>*</font></html>");
		lblAnoDeAquisicao.setFont(new Font("Tahoma", Font.PLAIN, 14));
		lblAnoDeAquisicao.setBounds(9, 219, 129, 14);
		contentPane.add(lblAnoDeAquisicao);
		
		JLabel lblPrecoDeAquisicao = new JLabel("<html>Pre\u00E7o de Aquisi\u00E7\u00E3o : <font color='red'>*</font></html>");
		lblPrecoDeAquisicao.setFont(new Font("Tahoma", Font.PLAIN, 14));
		lblPrecoDeAquisicao.setBounds(10, 254, 141, 14);
		contentPane.add(lblPrecoDeAquisicao);
		
		JLabel lblAnoDeVenda = new JLabel("Ano de Venda :");
		lblAnoDeVenda.setFont(new Font("Tahoma", Font.PLAIN, 14));
		lblAnoDeVenda.setBounds(10, 290, 105, 14);
		contentPane.add(lblAnoDeVenda);
		
		JLabel lblValorDeVenda = new JLabel("Valor de Venda :");
		lblValorDeVenda.setFont(new Font("Tahoma", Font.PLAIN, 14));
		lblValorDeVenda.setBounds(10, 325, 105, 14);
		contentPane.add(lblValorDeVenda);
		
		txt_marca = new JTextField();
		txt_marca.setBounds(87, 56, 204, 20);
		contentPane.add(txt_marca);
		txt_marca.setColumns(10);
		
		txt_modelo = new JTextField();
		txt_modelo.setColumns(10);
		txt_modelo.setBounds(88, 87, 204, 20);
		contentPane.add(txt_modelo);
		
		txt_cor = new JTextField();
		txt_cor.setColumns(10);
		txt_cor.setBounds(87, 118, 204, 20);
		contentPane.add(txt_cor);
		
		txt_ano_aquisicao = new JFormattedTextField();
		txt_ano_aquisicao.setHorizontalAlignment(SwingConstants.RIGHT);
		txt_ano_aquisicao.setColumns(10);
		txt_ano_aquisicao.setBounds(159, 217, 132, 20);
		Funcoes.adicionarMask("####", txt_ano_aquisicao);
		contentPane.add(txt_ano_aquisicao);
		
		txt_preco_aquisicao = new JSpinner(new SpinnerNumberModel(0, 0, 10000000, 100));
		txt_preco_aquisicao.setBounds(159, 251, 133, 20);
		JSpinner.NumberEditor jsEditor3 = (JSpinner.NumberEditor) txt_preco_aquisicao.getEditor();
		DefaultFormatter formatter3 = (DefaultFormatter) jsEditor3.getTextField().getFormatter();
		formatter3.setAllowsInvalid(false);
		contentPane.add(txt_preco_aquisicao);
		
		txt_ano_venda = new JFormattedTextField();
		txt_ano_venda.setHorizontalAlignment(SwingConstants.RIGHT);
		txt_ano_venda.setColumns(10);
		txt_ano_venda.setBounds(127, 290, 165, 20);
		Funcoes.adicionarMask("####", txt_ano_venda);
		contentPane.add(txt_ano_venda);
		
		txt_valor_venda = new JSpinner(new SpinnerNumberModel(0, 0, 10000000, 100));
		txt_valor_venda.setBounds(129, 323, 162, 20);
		JSpinner.NumberEditor jsEditor4 = (JSpinner.NumberEditor) txt_preco_aquisicao.getEditor();
		DefaultFormatter formatter4 = (DefaultFormatter) jsEditor4.getTextField().getFormatter();
		formatter4.setAllowsInvalid(false);
		contentPane.add(txt_valor_venda);
	
		
		txt_combustivel = new JComboBox<String>();
		txt_combustivel.setBounds(110, 184, 181, 20);
		contentPane.add(txt_combustivel);
		
		JButton btnGuardarCarro = new JButton("Guardar Carro");
		btnGuardarCarro.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent arg0) {
				String ano_venda = txt_ano_venda.getText().replaceAll("\\s+", "");//substituir espa�os por nada...
				String valor_venda = txt_valor_venda.getValue().toString().trim();
				
				if ( ano_venda.isEmpty() )
					ano_venda = "0";
				
				if ( valor_venda.isEmpty() )
					valor_venda = "0";
				
				if (txt_marca.getText().isEmpty()//verificar se algum campo obrigat�rio est� vazio
					|| txt_modelo.getText().isEmpty()
					|| txt_cor.getText().isEmpty()
					|| txt_ano_aquisicao.getText().trim().isEmpty()
					|| Integer.parseInt(txt_preco_aquisicao.getValue().toString()) == 0)
					JOptionPane.showMessageDialog(null, "Insira dados nos campos obrigat�rios", "Caixas de texto vazias", JOptionPane.INFORMATION_MESSAGE);
				else
				{
					if (Integer.parseInt(txt_ano_aquisicao.getText().trim()) > Integer.parseInt(ano_venda) && Integer.parseInt(ano_venda) != 0)//verificar se o ano de aquisi��o � maior que o de venda
						JOptionPane.showMessageDialog(null, "Ano de aquisicao n�o pode ser maior que o de venda", "Ano Inv�lido",JOptionPane.INFORMATION_MESSAGE);
					else
					{
						if(txt_ano_aquisicao.getText().trim().length() != 4)
							JOptionPane.showMessageDialog(null, "Ano de aquisi��o � composto por quatro valores", "Ano de Aquisi��o Inv�lido",JOptionPane.INFORMATION_MESSAGE);
						else
						{
							try
							{
								if (Integer.parseInt(txt_ano_aquisicao.getText().trim()) > Calendar.getInstance().get(Calendar.YEAR) || Integer.parseInt(ano_venda) > Calendar.getInstance().get(Calendar.YEAR))
									JOptionPane.showMessageDialog(null, "Um dos anos inseridos � maior do que o ano atual", "Ano Inv�lido", JOptionPane.INFORMATION_MESSAGE);
								else
								{
									if(Integer.parseInt(ano_venda) != 0 && Double.parseDouble(valor_venda) == 0)
										JOptionPane.showMessageDialog(null, "Insira o valor de venda", "Valor de Venda Vazio", JOptionPane.INFORMATION_MESSAGE);
									else
									{
										String alterar = "UPDATE Carro SET " +
												"Marca='" + txt_marca.getText() + "', Modelo='" + txt_modelo.getText() + "', Cor='" + txt_cor.getText() + "', "+
												"Cilindrada=" + txt_cilindrada.getValue().toString() + ", Combustivel='" + txt_combustivel.getSelectedItem().toString() + "', "+
												"AnoAquisicao=" + txt_ano_aquisicao.getText() + ", PrecoAquisicao=" + txt_preco_aquisicao.getValue().toString() + ",AnoVenda=" + ano_venda + ",ValorVenda=" + valor_venda+
												" WHERE Matricula LIKE '" + lbl_matricula.getText() + "'";
										rs.close();
										stmt.executeUpdate(alterar);
										JOptionPane.showMessageDialog(null, "Carro Alterado Com Sucesso!", "Carro Alterado", JOptionPane.INFORMATION_MESSAGE);
										dispose();
									}
								}
							} catch (Exception e) {
								System.out.println(e.getMessage());
							}
						}
					}
				}
			}
		});
		btnGuardarCarro.setBounds(151, 379, 140, 23);
		btnGuardarCarro.setFocusPainted(false);
		btnGuardarCarro.setContentAreaFilled(false);
		contentPane.add(btnGuardarCarro);
		
		txt_cilindrada = new JSpinner(new SpinnerNumberModel(500, 500, 2000, 100));
		txt_cilindrada.setBounds(95, 154, 196, 20);
		txt_cilindrada.setEditor(new JSpinner.NumberEditor(txt_cilindrada, "0000"));
		JSpinner.NumberEditor jsEditor = (JSpinner.NumberEditor) txt_cilindrada.getEditor();
		DefaultFormatter formatter = (DefaultFormatter) jsEditor.getTextField().getFormatter();
		formatter.setAllowsInvalid(false);
		contentPane.add(txt_cilindrada);
		
		JLabel label = new JLabel("");
		label.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent arg0) {
				String resp = JOptionPane.showInputDialog(null, "Insira o novo nome do combustivel:", "Novo Combustivel", JOptionPane.PLAIN_MESSAGE);
				try
				{
					rs = stmt.executeQuery("SELECT UPPER(NomeCombustivel) FROM Combustiveis WHERE NomeCombustivel LIKE '" + resp.toUpperCase() + "'");
					if(resp != null && !resp.isEmpty() && !rs.isBeforeFirst())
					{
						stmt.executeUpdate("INSERT INTO Combustiveis(NomeCombustivel)VALUES('" + resp + "')");
						txt_combustivel.removeAllItems();
						rs = stmt.executeQuery("SELECT NomeCombustivel FROM Combustiveis");
						while (rs.next())
							txt_combustivel.addItem(rs.getString(1));
					}
					else
						JOptionPane.showMessageDialog(null, "Combustivel Inv�lido ou j� est� inserido na base de dados!", "Combustivel Inv�lido", JOptionPane.INFORMATION_MESSAGE);
				}catch(SQLException se){
					se.printStackTrace();
				}
			}
		});
		label.setIcon(new ImageIcon("C:\\Users\\Jorge\\Downloads\\plus.png"));
		label.setBounds(297, 186, 16, 16);
		contentPane.add(label);
		
		Dimension dimension = Toolkit.getDefaultToolkit().getScreenSize();
		int x = (int) ((dimension.getWidth() - this.getWidth()) / 2);
		int y = (int) ((dimension.getHeight() - this.getHeight()) / 2);
		setLocation(x, y);
		setResizable(false);
		
	}
	
	public void setLbl_matricula(String lbl_matricula){
		this.lbl_matricula.setText(lbl_matricula);
		String combustivel = "";
		try{
			rs = stmt.executeQuery("SELECT * FROM Carro WHERE Matricula LIKE '" + lbl_matricula + "'");
			if(rs.next())
			{
				txt_marca.setText(rs.getString(1));
				txt_modelo.setText(rs.getString(2));
				txt_cor.setText(rs.getString(3));
				txt_cilindrada.setValue(rs.getInt(4));
				combustivel = rs.getString(6);
				txt_ano_aquisicao.setText(rs.getString(7));
				txt_preco_aquisicao.setValue(rs.getInt(8));
				txt_ano_venda.setText(rs.getString(9));
				txt_valor_venda.setValue(rs.getInt(10));
			}
			rs = stmt.executeQuery("SELECT * FROM Combustiveis");
			while (rs.next())
				txt_combustivel.addItem(rs.getString(1));
			txt_combustivel.setSelectedItem(combustivel);
		}catch(Exception e){
			e.printStackTrace();
		}
	}
}
