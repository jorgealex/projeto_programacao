import java.awt.*;
import javax.swing.*;
import java.awt.event.*;

public class MainForm extends JFrame {


	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					MainForm frame = new MainForm();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public MainForm() {
		setTitle("Main");
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setIconImage(new ImageIcon("menu.png").getImage());
		setBounds(100, 100, 233, 410);

		Dimension dimension = Toolkit.getDefaultToolkit().getScreenSize();
		int x = (int) ((dimension.getWidth() - this.getWidth()) / 2);
		int y = (int) ((dimension.getHeight() - this.getHeight()) / 2);
		setLocation(x, y);
		setResizable(false);

		getContentPane().setLayout(null);

		JButton btnConsultarCarros = new JButton("Consultar e Editar Carros");
		btnConsultarCarros.setBounds(10, 176, 207, 35);
		getContentPane().add(btnConsultarCarros);
		btnConsultarCarros.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				try {
					ConsultarCarros cc = new ConsultarCarros();
					cc.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
					cc.setVisible(true);
				} catch (Exception e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}

			}
		});
		btnConsultarCarros.setFocusPainted(false);
		btnConsultarCarros.setContentAreaFilled(false);

		JButton btnInserirCarro = new JButton("Inserir Carro");
		btnInserirCarro.setBounds(10, 232, 207, 35);
		getContentPane().add(btnInserirCarro);
		btnInserirCarro.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				InserirCarro ic = new InserirCarro();
				ic.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
				ic.setVisible(true);
			}
		});
		btnInserirCarro.setFocusPainted(false);
		btnInserirCarro.setContentAreaFilled(false);

		JButton btnTaxas = new JButton("Taxas");
		btnTaxas.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				Taxas t = new Taxas();
				t.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
				t.setVisible(true);
			}
		});
		btnTaxas.setBounds(10, 288, 207, 35);
		getContentPane().add(btnTaxas);
		btnTaxas.setFocusPainted(false);
		btnTaxas.setContentAreaFilled(false);

		JLabel lbPicture = new JLabel("");
		lbPicture.setIcon(new ImageIcon("menu.png"));
		lbPicture.setBounds(50, 11, 128, 128);
		getContentPane().add(lbPicture);
	}
}
