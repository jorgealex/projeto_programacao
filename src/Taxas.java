import java.awt.*;
import java.sql.*;
import java.util.*;
import java.util.List;

import javax.swing.*;
import javax.swing.text.DefaultFormatter;

import java.awt.event.*;
import javax.swing.table.TableModel;
import javax.swing.event.ChangeListener;
import javax.swing.event.ChangeEvent;

public class Taxas extends JFrame {

	public static final String JDBC_DRIVER = "org.sqlite.JDBC";
	public static final String DB_URL = "jdbc:sqlite:projeto.db";
	public static Connection conn = null;
	public static Statement stmt = null;
	public static ResultSet rs;
	Funcoes f = new Funcoes();
	ButtonGroup bg = new ButtonGroup(), bgEditar = new ButtonGroup();

	private JPanel contentPane;
	private JPanel panel_consultar;
	private JPanel panel_inspecao;
	private JPanel panel_iuc;
	private JPanel panel_revisao;
	private JPanel panel_seguro;
	private JLabel lb_obs_inspecao_counter;
	private JLabel lb_obs_revisao_counter;
	private JTable table;
	private JComboBox<String> combo_taxa_consultar;
	private JSpinner txt_editar_valor_inspecao;
	private JPanel panel_editar_inspecao;
	private JSpinner txt_editar_valor_iuc;
	private JComboBox<String> combo_editar_matricula_iuc;
	private JButton btnEditarIUC;
	private JComboBox<String> combo_editar_matricula_inspecao;
	private JLabel lb_editar_data_inspecao;
	private JLabel lb_editar_valor_iuc;
	private JLabel lb_editar_obs_inspecao;
	private JLabel lb_editar_matricula_iuc;
	private JLabel lb_editar_passou;
	private JLabel lb_editar_matricula_inspecao;
	private JPanel panel_editar_iuc;
	private JFormattedTextField txt_editar_data_iuc;
	private JFormattedTextField txt_editar_data_inspecao;
	private JRadioButton rdbEditarNaoInspecao;
	private JButton btnEliminarInspecao;
	private JLabel lb_editar_datavalidade_iuc;
	private JLabel lb_editar_obs_inspecao_counter;
	private JLabel lb_editar_valor;
	private JScrollPane scrollPaneEditarInspecao;
	private JButton btnEliminarIUC;
	private JRadioButton rdbEditarSimInspecao;
	private JButton btnEditarInspecao;
	private JTextArea txt_editar_obs_inspecao;
	private JTextField txt_editar_oficina_revisao;
	private JButton btnEditarRevisao;
	private JTextArea txt_editar_obs_revisao;
	private JSpinner txt_editar_kms_revisao;
	private JLabel lb_editar_datarevisao;
	private JSpinner txt_editar_valor_revisao;
	private JLabel lb_editar_kms_revisao;
	private JLabel lb_editar_valor_revisao;
	private JLabel lb_editar_matricula_revisao;
	private JLabel lb_editar_obs_revisao_counter;
	private JPanel panel_editar_revisao;
	private JButton btnEliminarRevisao;
	private JFormattedTextField txt_editar_data_revisao;
	private JLabel lb_editar_oficina_revisao;
	private JLabel lb_editar_obs_revisao;
	private JComboBox<String> combo_editar_matricula_revisao;
	private JPanel panel_editar_seguro;
	private JTextField txt_editar_nome_seguro;
	private JLabel lb_editar_matricula_seguro;
	private JButton btnEliminarSeguro;
	private JFormattedTextField txt_editar_data_seguro;
	private JLabel label;
	private JLabel lb_editar_data_seguro;
	private JLabel lb_editar_nome_seguro;
	private JSpinner txt_editar_valor_seguro;
	private JButton btnEditarSeugo;
	private JComboBox<String> combo_editar_matricula_seguro;
	private JPanel panel_total;
	private JLabel lb_matricula_total;
	private JComboBox<String> combo_total_matricula;
	private JLabel lblTotalDasInspees;
	private JLabel lblTotalDosIucs;
	private JLabel lblTotalDasRevises;
	private JLabel lblTotalDoSeguro;
	private JTextField txt_total_inspecao;
	private JTextField txt_total_iuc;
	private JTextField txt_total_revisao;
	private JTextField txt_total_seguro;
	private JButton btnCalcularTotal;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					Taxas frame = new Taxas();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public Taxas() {
		try {
			Class.forName(JDBC_DRIVER);
			conn = DriverManager.getConnection(DB_URL);
			stmt = conn.createStatement();
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
		} catch (SQLException se) {
			se.printStackTrace();
		}

		setTitle("Taxas");
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setIconImage(new ImageIcon("taxas.png").getImage());
		setBounds(100, 100, 480, 360);
		
		Dimension dimension = Toolkit.getDefaultToolkit().getScreenSize();
		int x = (int) ((dimension.getWidth() - this.getWidth()) / 2);
		int y = (int) ((dimension.getHeight() - this.getHeight()) / 2);
		setLocation(x, y);
		setResizable(false);
		
		contentPane = new JPanel();
		setContentPane(contentPane);
		contentPane.setLayout(null);
		addComponentListener( new ComponentAdapter () {
			public void componentShown ( ComponentEvent e )
	        {
				setSize(520, 400);
	        }
		});
		JTabbedPane tabbedPane = new JTabbedPane(JTabbedPane.TOP);
		tabbedPane.addChangeListener(new ChangeListener() {
			public void stateChanged(ChangeEvent arg0) {
				try
				{
					if (tabbedPane.getSelectedIndex() == 0)
					{
						if(panel_inspecao != null && panel_iuc != null && panel_revisao != null && panel_seguro != null)
						{
							panel_inspecao.setVisible(true);
							panel_iuc.setVisible(false);
							panel_revisao.setVisible(false);
							panel_seguro.setVisible(false);
							setSize(520, 400);
						}
					}
					else if (tabbedPane.getSelectedIndex() == 1)
					{
						setSize(850, 520);
						panel_editar_inspecao.setVisible(true);
						panel_editar_iuc.setVisible(false);
						panel_editar_revisao.setVisible(false);
						panel_editar_seguro.setVisible(false);
						rs = stmt.executeQuery("SELECT * FROM " + combo_taxa_consultar.getSelectedItem());
						table.setModel(Funcoes.buildTableModel(rs));
					}
					else if (tabbedPane.getSelectedIndex() == 2)
					{
						setSize(480, 360);
						combo_total_matricula.removeAllItems();
						preencherCB(combo_total_matricula);
					}
					int x = (int) ((dimension.getWidth() - getWidth()) / 2);
					int y = (int) ((dimension.getHeight() - getHeight()) / 2);
					setLocation(x, y);
			}
			catch(Exception ex)
			{
				ex.printStackTrace();
			}
		}});
		tabbedPane.setBounds(0, 0, 844, 492);
		contentPane.add(tabbedPane);

		JPanel panel_adicioar = new JPanel();
		tabbedPane.addTab("Adicionar Taxa", null, panel_adicioar, null);
		panel_adicioar.setLayout(null);

		JComboBox<String> combo_taxa = new JComboBox<String>();
		combo_taxa.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				Object selected = combo_taxa.getSelectedItem();
				if (selected.toString().equals("Inspecao")) {
					panel_inspecao.setVisible(true);
					panel_iuc.setVisible(false);
					panel_revisao.setVisible(false);
					panel_seguro.setVisible(false);
					setSize(520, 400);
				}
				if (selected.toString().equals("IUC")) {
					panel_inspecao.setVisible(false);
					panel_iuc.setVisible(true);
					panel_revisao.setVisible(false);
					panel_seguro.setVisible(false);
					setSize(520, 260);
				}
				if (selected.toString().equals("Revisao")) {
					panel_inspecao.setVisible(false);
					panel_iuc.setVisible(false);
					panel_revisao.setVisible(true);
					panel_seguro.setVisible(false);
					setSize(520, 425);
				}
				if (selected.toString().equals("Seguro")) {
					panel_inspecao.setVisible(false);
					panel_iuc.setVisible(false);
					panel_revisao.setVisible(false);
					panel_seguro.setVisible(true);
					setSize(520, 280);
				}
			}
		});
		combo_taxa.setBounds(228, 10, 90, 20);
		panel_adicioar.add(combo_taxa);
		combo_taxa.setModel(new DefaultComboBoxModel(new String[] { "Inspecao", "IUC", "Revisao", "Seguro" }));
		((JLabel) combo_taxa.getRenderer()).setHorizontalAlignment(JLabel.CENTER);

		JLabel lblTipoDeTaxa = new JLabel("Tipo de Taxa :");
		lblTipoDeTaxa.setBounds(120, 12, 98, 15);
		panel_adicioar.add(lblTipoDeTaxa);
		lblTipoDeTaxa.setFont(new Font("Trebuchet MS", Font.PLAIN, 14));

		panel_inspecao = new JPanel();
		panel_inspecao.setBounds(0, 41, 509, 342);
		panel_adicioar.add(panel_inspecao);
		panel_inspecao.setLayout(null);
		
		JLabel lb_matricula_inspecao = new JLabel("<html> Matr\u00EDcula : <font color='red'>*</font></html>");
		lb_matricula_inspecao.setFont(new Font("Trebuchet MS", Font.PLAIN, 14));
		lb_matricula_inspecao.setBounds(130, 13, 76, 18);
		panel_inspecao.add(lb_matricula_inspecao);

		JLabel data_inspecao = new JLabel("<html> Data de Inspe��o : <font color='red'>*</font></html>");
		data_inspecao.setFont(new Font("Trebuchet MS", Font.PLAIN, 14));
		data_inspecao.setBounds(79, 42, 127, 18);
		panel_inspecao.add(data_inspecao);

		JFormattedTextField txt_data_inspecao = new JFormattedTextField();
		txt_data_inspecao.setHorizontalAlignment(SwingConstants.RIGHT);
		txt_data_inspecao.setFont(new Font("Trebuchet MS", Font.PLAIN, 14));
		txt_data_inspecao.setColumns(10);
		txt_data_inspecao.setBounds(216, 42, 116, 21);
		Funcoes.adicionarMask("##-##-####", txt_data_inspecao);
		txt_data_inspecao.setToolTipText("dd/MM/yyyy");
		panel_inspecao.add(txt_data_inspecao);

		JLabel lblObservacoesInspecao = new JLabel("<html> Observa\u00E7\u00F5es : <font color='red'>*</font></html>");
		lblObservacoesInspecao.setFont(new Font("Trebuchet MS", Font.PLAIN, 14));
		lblObservacoesInspecao.setBounds(109, 71, 97, 18);
		panel_inspecao.add(lblObservacoesInspecao);

		JTextArea txt_obs_inspecao = new JTextArea();
		txt_obs_inspecao.addKeyListener(new KeyAdapter() {
			@Override
			public void keyTyped(KeyEvent e) {
				if (txt_obs_inspecao.getText().length() <= 200)
					lb_obs_inspecao_counter.setForeground(Color.green);
				else
					lb_obs_inspecao_counter.setForeground(Color.red);
				lb_obs_inspecao_counter.setText("Letras usadas : " + txt_obs_inspecao.getText().length() + "/200!");
			}
		});
		txt_obs_inspecao.setLineWrap(true);
		txt_obs_inspecao.setFont(new Font("Trebuchet MS", Font.PLAIN, 14));
		txt_obs_inspecao.setBounds(216, 74, 283, 98);

		JScrollPane scrollInspecao = new JScrollPane(txt_obs_inspecao);
		scrollInspecao.setBounds(216, 74, 283, 98);
		panel_inspecao.add(scrollInspecao);

		JLabel lblValorInspecao = new JLabel("<html> Valor : <font color='red'>*</font></html>");
		lblValorInspecao.setFont(new Font("Trebuchet MS", Font.PLAIN, 14));
		lblValorInspecao.setBounds(156, 189, 50, 18);
		panel_inspecao.add(lblValorInspecao);

		JSpinner txt_valor_inspecao = new JSpinner(new SpinnerNumberModel(0.00, 0.00, 1000.00, 10.00));
		txt_valor_inspecao.setFont(new Font("Trebuchet MS", Font.PLAIN, 12));
		txt_valor_inspecao.setBounds(216, 186, 116, 22);
		modificarSpinners(txt_valor_inspecao);
		panel_inspecao.add(txt_valor_inspecao);

		JLabel lblPassouInspecao = new JLabel("<html> Passou : <font color='red'>*</font></html>");
		lblPassouInspecao.setFont(new Font("Trebuchet MS", Font.PLAIN, 14));
		lblPassouInspecao.setBounds(145, 218, 61, 18);
		panel_inspecao.add(lblPassouInspecao);

		JRadioButton rdbtnSimInspecao = new JRadioButton("Sim");
		rdbtnSimInspecao.setFont(new Font("Tahoma", Font.PLAIN, 14));
		rdbtnSimInspecao.setBounds(216, 217, 50, 19);
		panel_inspecao.add(rdbtnSimInspecao);

		JRadioButton rdbtnNaoInspecao = new JRadioButton("N\u00E3o");
		rdbtnNaoInspecao.setFont(new Font("Tahoma", Font.PLAIN, 14));
		rdbtnNaoInspecao.setBounds(282, 217, 50, 19);
		panel_inspecao.add(rdbtnNaoInspecao);

		bg.add(rdbtnSimInspecao);
		bg.add(rdbtnNaoInspecao);

		JComboBox<String> txt_matricula_inspecao = new JComboBox<String>();
		txt_matricula_inspecao.setBounds(216, 13, 116, 20);
		panel_inspecao.add(txt_matricula_inspecao);
		preencherCB(txt_matricula_inspecao);

		JButton btnInserirInspecao = new JButton("Inserir");
		btnInserirInspecao.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent arg0) {
				String data = txt_data_inspecao.getText(), passou = "";
				if (data.replaceAll("-", "").trim().isEmpty())
					JOptionPane.showMessageDialog(null, "Data Vazia", "Data Vazia", JOptionPane.ERROR_MESSAGE);
				else {
					int dia, mes, ano;
					if (data.replaceAll("-", "").trim().length() != 8)
						JOptionPane.showMessageDialog(null, "Data Incompleta", "Data Incompleta",
								JOptionPane.ERROR_MESSAGE);
					else {
						dia = Integer.parseInt(data.substring(0, data.indexOf("-")).trim());
						if (dia <= 0 || dia >= 32)
							JOptionPane.showMessageDialog(null, "Dia Inv�lido", "Dia Inv�lido!",
									JOptionPane.ERROR_MESSAGE);
						else {
							mes = Integer.parseInt(data.substring(data.indexOf("-") + 1, data.lastIndexOf("-")).trim());
							if (mes <= 0 || mes >= 13)
								JOptionPane.showMessageDialog(null, "M�s Inv�lido", "M�s Inv�lido!",
										JOptionPane.ERROR_MESSAGE);
							else {
								ano = Integer.parseInt(data.substring(data.lastIndexOf("-") + 1, data.length()).trim());
								if (ano <= 1900 || ano > Calendar.getInstance().get(Calendar.YEAR))
									JOptionPane.showMessageDialog(null, "Ano Inv�lido", "Ano Inv�lido!",
											JOptionPane.ERROR_MESSAGE);
								else {
									if (txt_obs_inspecao.getText().length() > 200)
										JOptionPane.showMessageDialog(null, "Observa��o demasiado grande",
												"Observa��o Inv�lida!", JOptionPane.ERROR_MESSAGE);
									else if (txt_obs_inspecao.getText().isEmpty())
										JOptionPane.showMessageDialog(null, "Observa��o vazia", "Observa��o Inv�lida!",
												JOptionPane.ERROR_MESSAGE);
									else {
										if (Double.parseDouble(txt_valor_inspecao.getValue().toString()) == 0)
											JOptionPane.showMessageDialog(null, "Valor da inspe��o n�o pode ser 0!",
													"Valor Inv�lido!", JOptionPane.ERROR_MESSAGE);
										else {
											if (!rdbtnSimInspecao.isSelected() && !rdbtnNaoInspecao.isSelected())
												JOptionPane.showMessageDialog(null,
														"Selecione um resultado se passou ou n�o",
														"Resultado Inv�lido!", JOptionPane.ERROR_MESSAGE);
											else {
												if (rdbtnSimInspecao.isSelected())
													passou = "Sim";
												else if (rdbtnNaoInspecao.isSelected())
													passou = "N�o";
												try {
													String insert = "INSERT INTO Inspecao(Matricula,DataInspec,Obs,Valor,Passou)VALUES('"
															+ txt_matricula_inspecao.getSelectedItem() + "','"
															+ txt_data_inspecao.getText() + "','"
															+ txt_obs_inspecao.getText() + "',"
															+ txt_valor_inspecao.getValue().toString() + ",'" + passou
															+ "')";
													rs.close();
													stmt.executeUpdate(insert);
													JOptionPane.showMessageDialog(null,
															"Inspe��o adicionada com sucesso!", "Inspe��o Inserido",
															JOptionPane.INFORMATION_MESSAGE);
													txt_data_inspecao.setText("");
													txt_valor_inspecao.setValue(0);
													txt_obs_inspecao.setText("");
													bg.clearSelection();
													lb_obs_inspecao_counter.setText("Letras usadas : 0/200!");
												} catch (SQLException e) {
													e.printStackTrace();
												}
											}
										}
									}
								}
							}
						}
					}
				}

			}
		});
		btnInserirInspecao.setBounds(216, 264, 89, 23);
		btnInserirInspecao.setFocusPainted(false);
		btnInserirInspecao.setContentAreaFilled(false);
		panel_inspecao.add(btnInserirInspecao);

		lb_obs_inspecao_counter = new JLabel("Letras usadas : 0/200!");
		lb_obs_inspecao_counter.setHorizontalAlignment(SwingConstants.LEFT);
		lb_obs_inspecao_counter.setForeground(Color.GREEN);
		lb_obs_inspecao_counter.setFont(new Font("Trebuchet MS", Font.PLAIN, 12));
		lb_obs_inspecao_counter.setBounds(364, 172, 135, 14);
		panel_inspecao.add(lb_obs_inspecao_counter);

		panel_inspecao.setVisible(true);

		panel_iuc = new JPanel();
		panel_iuc.setBounds(0, 41, 509, 342);
		panel_adicioar.add(panel_iuc);
		panel_iuc.setLayout(null);

		JLabel lb_matricula_iuc = new JLabel("<html> Matr\u00EDcula : <font color='red'>*</font></html>");
		lb_matricula_iuc.setBounds(130, 13, 76, 18);
		lb_matricula_iuc.setFont(new Font("Trebuchet MS", Font.PLAIN, 14));
		panel_iuc.add(lb_matricula_iuc);

		JLabel lblDataDeValidadeIUC = new JLabel("<html> Data de Validade : <font color='red'>*</font></html>");
		lblDataDeValidadeIUC.setFont(new Font("Trebuchet MS", Font.PLAIN, 14));
		lblDataDeValidadeIUC.setBounds(79, 42, 127, 18);
		panel_iuc.add(lblDataDeValidadeIUC);

		JFormattedTextField txt_data_iuc = new JFormattedTextField();
		txt_data_iuc.setHorizontalAlignment(SwingConstants.RIGHT);
		txt_data_iuc.setFont(new Font("Trebuchet MS", Font.PLAIN, 14));
		txt_data_iuc.setColumns(10);
		txt_data_iuc.setBounds(216, 42, 115, 21);
		Funcoes.adicionarMask("##-##-####", txt_data_iuc);
		panel_iuc.add(txt_data_iuc);

		JLabel lb_valor_iuc = new JLabel("<html> Valor : <font color='red'>*</font></html>");
		lb_valor_iuc.setFont(new Font("Trebuchet MS", Font.PLAIN, 14));
		lb_valor_iuc.setBounds(156, 71, 50, 18);
		panel_iuc.add(lb_valor_iuc);

		JSpinner txt_valor_iuc = new JSpinner(new SpinnerNumberModel(0, 0, 1000, 10));
		txt_valor_iuc.setFont(new Font("Trebuchet MS", Font.PLAIN, 12));
		txt_valor_iuc.setBounds(216, 71, 116, 22);
		modificarSpinners(txt_valor_iuc);
		panel_iuc.add(txt_valor_iuc);

		JComboBox<String> txt_matricula_iuc = new JComboBox<String>();
		txt_matricula_iuc.setBounds(216, 13, 115, 20);
		preencherCB(txt_matricula_iuc);
		panel_iuc.add(txt_matricula_iuc);
		panel_iuc.setVisible(false);
		

		JButton btnInserirIUC = new JButton("Inserir");
		btnInserirIUC.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent arg0) {
				String data = txt_data_iuc.getText();
				if (data.replaceAll("-", "").trim().isEmpty())
					JOptionPane.showMessageDialog(null, "Data Vazia", "Data Vazia", JOptionPane.ERROR_MESSAGE);
				else {
					int dia, mes, ano;
					if (data.replaceAll("-", "").trim().length() != 8)
						JOptionPane.showMessageDialog(null, "Data Incompleta", "Data Incompleta",
								JOptionPane.ERROR_MESSAGE);
					else {
						dia = Integer.parseInt(data.substring(0, data.indexOf("-")).trim());
						if (dia <= 0 || dia >= 32)
							JOptionPane.showMessageDialog(null, "Dia Inv�lido", "Dia Inv�lido!",
									JOptionPane.ERROR_MESSAGE);
						else {
							mes = Integer.parseInt(data.substring(data.indexOf("-") + 1, data.lastIndexOf("-")).trim());
							if (mes <= 0 || mes >= 13)
								JOptionPane.showMessageDialog(null, "M�s Inv�lido", "M�s Inv�lido!",
										JOptionPane.ERROR_MESSAGE);
							else {
								ano = Integer.parseInt(data.substring(data.lastIndexOf("-") + 1, data.length()).trim());
								if (ano <= 1900 || ano > Calendar.getInstance().get(Calendar.YEAR))
									JOptionPane.showMessageDialog(null, "Ano Inv�lido", "Ano Inv�lido!",
											JOptionPane.ERROR_MESSAGE);
								else {
									if (Double.parseDouble(txt_valor_iuc.getValue().toString()) == 0)
										JOptionPane.showMessageDialog(null, "Valor de IUC n�o pode ser 0!",
												"Valor Inv�lido!", JOptionPane.ERROR_MESSAGE);
									else {
										try {
											String insert = "INSERT INTO IUC(Matricula,DataValidade,Valor)VALUES('"
													+ txt_matricula_iuc.getSelectedItem() + "','"
													+ txt_data_iuc.getText() + "'," + txt_valor_iuc.getValue().toString()
													+ ")";
											stmt.executeUpdate(insert);
											JOptionPane.showMessageDialog(null, "IUC adicionado com sucesso!",
													"IUC Inserido", JOptionPane.INFORMATION_MESSAGE);
											txt_data_iuc.setText("");
											txt_valor_iuc.setValue(0);
										} catch (SQLException e) {
											e.printStackTrace();
										}
									}
								}
							}
						}
					}
				}
			}
		});
		btnInserirIUC.setFocusPainted(false);
		btnInserirIUC.setContentAreaFilled(false);
		btnInserirIUC.setBounds(216, 122, 116, 23);
		panel_iuc.add(btnInserirIUC);

		panel_revisao = new JPanel();
		panel_revisao.setForeground(Color.BLACK);
		panel_revisao.setBounds(0, 41, 509, 342);
		panel_revisao.setLayout(null);
		panel_adicioar.add(panel_revisao);

		JLabel lblOficinaRevisao = new JLabel("<html> Oficina : <font color='red'>*</font></html>");
		lblOficinaRevisao.setFont(new Font("Trebuchet MS", Font.PLAIN, 14));
		lblOficinaRevisao.setBounds(144, 13, 62, 18);
		panel_revisao.add(lblOficinaRevisao);

		JFormattedTextField txt_oficina_revisao = new JFormattedTextField();
		txt_oficina_revisao.setHorizontalAlignment(SwingConstants.RIGHT);
		txt_oficina_revisao.setFont(new Font("Trebuchet MS", Font.PLAIN, 14));
		txt_oficina_revisao.setColumns(10);
		txt_oficina_revisao.setBounds(216, 11, 119, 21);
		panel_revisao.add(txt_oficina_revisao);

		JLabel lblDataDeRevisao = new JLabel("<html> Data de Revis\u00E3o : <font color='red'>*</font></html>");
		lblDataDeRevisao.setFont(new Font("Trebuchet MS", Font.PLAIN, 14));
		lblDataDeRevisao.setBounds(87, 75, 119, 18);
		panel_revisao.add(lblDataDeRevisao);

		JFormattedTextField txt_data_revisao = new JFormattedTextField();
		txt_data_revisao.setHorizontalAlignment(SwingConstants.RIGHT);
		txt_data_revisao.setFont(new Font("Trebuchet MS", Font.PLAIN, 14));
		txt_data_revisao.setColumns(10);
		txt_data_revisao.setBounds(216, 73, 119, 21);
		Funcoes.adicionarMask("##-##-####", txt_data_revisao);
		panel_revisao.add(txt_data_revisao);

		JLabel lb_obs_revisao = new JLabel("<html> Observa\u00E7\u00F5es : <font color='red'>*</font></html>");
		lb_obs_revisao.setFont(new Font("Trebuchet MS", Font.PLAIN, 14));
		lb_obs_revisao.setBounds(109, 102, 97, 18);
		panel_revisao.add(lb_obs_revisao);

		JTextArea txt_obs_revisao = new JTextArea();
		txt_obs_revisao.addKeyListener(new KeyAdapter() {
			@Override
			public void keyTyped(KeyEvent arg0) {
				if (txt_obs_revisao.getText().length() <= 200)
					lb_obs_revisao_counter.setForeground(Color.green);
				else
					lb_obs_revisao_counter.setForeground(Color.red);
				lb_obs_revisao_counter.setText("Letras usadas : " + txt_obs_revisao.getText().length() + "/200!");
			}
		});
		txt_obs_revisao.setLineWrap(true);
		txt_obs_revisao.setFont(new Font("Trebuchet MS", Font.PLAIN, 14));
		txt_obs_revisao.setBounds(0, 0, 281, 96);

		JScrollPane scrollObsRevisao = new JScrollPane(txt_obs_revisao);
		scrollObsRevisao.setBounds(216, 105, 283, 98);
		panel_revisao.add(scrollObsRevisao);

		JLabel lb_valor_revisao = new JLabel("<html> Valor : <font color='red'>*</font></html>");
		lb_valor_revisao.setFont(new Font("Trebuchet MS", Font.PLAIN, 14));
		lb_valor_revisao.setBounds(156, 249, 50, 18);
		panel_revisao.add(lb_valor_revisao);

		JSpinner txt_valor_revisao = new JSpinner(new SpinnerNumberModel(0.00, 0.00, 1000.00, 10.00));
		txt_valor_revisao.setFont(new Font("Trebuchet MS", Font.PLAIN, 12));
		txt_valor_revisao.setBounds(216, 246, 119, 22);
		modificarSpinners(txt_valor_revisao);
		panel_revisao.add(txt_valor_revisao);

		JLabel lblKmsRevisao = new JLabel("<html> KMs : <font color='red'>*</font></html>");
		lblKmsRevisao.setFont(new Font("Trebuchet MS", Font.PLAIN, 14));
		lblKmsRevisao.setBounds(163, 44, 43, 18);
		panel_revisao.add(lblKmsRevisao);

		JSpinner txt_kms_revisao = new JSpinner(new SpinnerNumberModel(0.00, 0.00, 9999999.00, 100.00));
		txt_kms_revisao.setFont(new Font("Trebuchet MS", Font.PLAIN, 12));
		txt_kms_revisao.setBounds(216, 40, 119, 22);
		modificarSpinners(txt_kms_revisao);
		panel_revisao.add(txt_kms_revisao);

		JLabel lblMatriculaRevisao = new JLabel("<html> Matricula : <font color='red'>*</font></html>");
		lblMatriculaRevisao.setFont(new Font("Trebuchet MS", Font.PLAIN, 14));
		lblMatriculaRevisao.setBounds(130, 216, 76, 18);
		panel_revisao.add(lblMatriculaRevisao);

		JComboBox<String> txt_matricula_revisao = new JComboBox<String>();
		txt_matricula_revisao.setBounds(216, 214, 119, 20);
		preencherCB(txt_matricula_revisao);
		panel_revisao.add(txt_matricula_revisao);

		lb_obs_revisao_counter = new JLabel("Letras usadas : 0/200!");
		lb_obs_revisao_counter.setHorizontalAlignment(SwingConstants.LEFT);
		lb_obs_revisao_counter.setForeground(Color.GREEN);
		lb_obs_revisao_counter.setFont(new Font("Trebuchet MS", Font.PLAIN, 12));
		lb_obs_revisao_counter.setBounds(364, 206, 135, 14);
		panel_revisao.add(lb_obs_revisao_counter);

		JButton btnInserirRevisao = new JButton("Inserir");
		btnInserirRevisao.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent arg0) {
				String data = txt_data_revisao.getText();
				if (txt_oficina_revisao.getText().isEmpty())
					JOptionPane.showMessageDialog(null, "Nome da oficina est� vazio", "Oficina Vazia",
							JOptionPane.ERROR_MESSAGE);
				else {
					if (data.replaceAll("-", "").trim().isEmpty())
						JOptionPane.showMessageDialog(null, "Data Vazia", "Data Vazia", JOptionPane.ERROR_MESSAGE);
					else {
						int dia, mes, ano;
						if (data.replaceAll("-", "").trim().length() != 8)
							JOptionPane.showMessageDialog(null, "Data Incompleta", "Data Incompleta",
									JOptionPane.ERROR_MESSAGE);
						else {
							dia = Integer.parseInt(data.substring(0, data.indexOf("-")).trim());
							if (dia <= 0 || dia >= 32)
								JOptionPane.showMessageDialog(null, "Dia Inv�lido", "Dia Inv�lido!",
										JOptionPane.ERROR_MESSAGE);
							else {
								mes = Integer
										.parseInt(data.substring(data.indexOf("-") + 1, data.lastIndexOf("-")).trim());
								if (mes <= 0 || mes >= 13)
									JOptionPane.showMessageDialog(null, "M�s Inv�lido", "M�s Inv�lido!",
											JOptionPane.ERROR_MESSAGE);
								else {
									ano = Integer
											.parseInt(data.substring(data.lastIndexOf("-") + 1, data.length()).trim());
									if (ano <= 1900 || ano > Calendar.getInstance().get(Calendar.YEAR))
										JOptionPane.showMessageDialog(null, "Ano Inv�lido", "Ano Inv�lido!",
												JOptionPane.ERROR_MESSAGE);
									else {
										if (Double.parseDouble(txt_kms_revisao.getValue().toString()) == 0)
											JOptionPane.showMessageDialog(null, "KMs n�o pode ser 0!", "KM",
													JOptionPane.ERROR_MESSAGE);
										else {
											if (txt_obs_revisao.getText().length() > 2000)
												JOptionPane.showMessageDialog(null, "Observa��o demasiado grande",
														"Observa��o Inv�lida!", JOptionPane.ERROR_MESSAGE);
											else if (txt_obs_revisao.getText().isEmpty())
												JOptionPane.showMessageDialog(null, "Observa��o vazia",
														"Observa��o Inv�lida!", JOptionPane.ERROR_MESSAGE);
											else {
												if (Double.parseDouble(txt_valor_revisao.getValue().toString()) == 0)
													JOptionPane.showMessageDialog(null,
															"Valor da revis�o n�o pode ser 0!", "Valor Inv�lido!",
															JOptionPane.ERROR_MESSAGE);
												else {
													try {
														String insert = "INSERT INTO Revisao(Oficina,Kms,DataRev,Obs,Matricula,Valor)VALUES('"
																+ txt_oficina_revisao.getText() + "',"
																+ txt_kms_revisao.getValue() + ",'"
																+ txt_data_revisao.getText() + "','"
																+ txt_obs_revisao.getText() + "','"
																+ txt_matricula_revisao.getSelectedItem() + "',"
																+ txt_valor_revisao.getValue() + ")";
														stmt.executeUpdate(insert);
														JOptionPane.showMessageDialog(null,
																"Revisao adicionado com sucesso!", "Revis�o Inserido",
																JOptionPane.INFORMATION_MESSAGE);
														txt_oficina_revisao.setText("");
														txt_kms_revisao.setValue(0);
														txt_data_revisao.setText("");
														txt_obs_revisao.setText("");
														txt_valor_revisao.setValue(0);
														lb_obs_revisao_counter.setText("Letras usadas : 0/200!");
													} catch (SQLException e) {
														e.printStackTrace();
													}
												}
											}
										}
									}
								}
							}
						}
					}
				}
			}
		});
		btnInserirRevisao.setFocusPainted(false);
		btnInserirRevisao.setContentAreaFilled(false);
		btnInserirRevisao.setBounds(217, 279, 118, 23);
		panel_revisao.add(btnInserirRevisao);

		panel_revisao.setVisible(false);
		
		panel_seguro = new JPanel();
		panel_seguro.setBounds(0, 41, 509, 342);
		panel_adicioar.add(panel_seguro);
		panel_seguro.setLayout(null);
		panel_seguro.setVisible(false);
		JLabel lbMatriculaSeguro = new JLabel("<html> Matr\u00EDcula : <font color='red'>*</font></html>");
		lbMatriculaSeguro.setFont(new Font("Trebuchet MS", Font.PLAIN, 14));
		lbMatriculaSeguro.setBounds(126, 106, 76, 18);
		panel_seguro.add(lbMatriculaSeguro);

		JLabel lbDataDeValidadeSeguro = new JLabel("<html> Data de Validade : <font color='red'>*</font></html>");
		lbDataDeValidadeSeguro.setFont(new Font("Trebuchet MS", Font.PLAIN, 14));
		lbDataDeValidadeSeguro.setBounds(76, 46, 127, 18);
		panel_seguro.add(lbDataDeValidadeSeguro);

		JFormattedTextField txt_data_seguro = new JFormattedTextField();
		txt_data_seguro.setHorizontalAlignment(SwingConstants.RIGHT);
		txt_data_seguro.setFont(new Font("Trebuchet MS", Font.PLAIN, 14));
		txt_data_seguro.setColumns(10);
		txt_data_seguro.setBounds(213, 46, 115, 21);
		Funcoes.adicionarMask("##-##-####", txt_data_seguro);
		panel_seguro.add(txt_data_seguro);

		JLabel lbValorSeguro = new JLabel("<html> Valor : <font color='red'>*</font></html>");
		lbValorSeguro.setFont(new Font("Trebuchet MS", Font.PLAIN, 14));
		lbValorSeguro.setBounds(153, 75, 50, 18);
		panel_seguro.add(lbValorSeguro);

		JSpinner txt_valor_seguro = new JSpinner(new SpinnerNumberModel(0.00, 0.00, 1000.00, 10.00));
		txt_valor_seguro.setFont(new Font("Trebuchet MS", Font.PLAIN, 12));
		txt_valor_seguro.setBounds(213, 75, 116, 22);
		modificarSpinners(txt_valor_seguro);
		panel_seguro.add(txt_valor_seguro);

		JLabel lblNomeSeguro = new JLabel("<html> Nome : <font color='red'>*</font></html>");
		lblNomeSeguro.setFont(new Font("Trebuchet MS", Font.PLAIN, 14));
		lblNomeSeguro.setBounds(148, 14, 54, 18);
		panel_seguro.add(lblNomeSeguro);

		JComboBox<String> txt_matricula_seguro = new JComboBox<String>();
		txt_matricula_seguro.setBounds(213, 106, 115, 20);
		preencherCB(txt_matricula_seguro);
		panel_seguro.add(txt_matricula_seguro);

		JFormattedTextField txt_nome_seguro = new JFormattedTextField();
		txt_nome_seguro.setHorizontalAlignment(SwingConstants.RIGHT);
		txt_nome_seguro.setFont(new Font("Trebuchet MS", Font.PLAIN, 14));
		txt_nome_seguro.setColumns(10);
		txt_nome_seguro.setBounds(212, 11, 116, 24);
		panel_seguro.add(txt_nome_seguro);

		JButton btnInserirSeguro = new JButton("Inserir");
		btnInserirSeguro.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				String data = txt_data_seguro.getText();
				if (txt_nome_seguro.getText().isEmpty())
					JOptionPane.showMessageDialog(null, "Nome est� vazio", "Nome Vazio", JOptionPane.ERROR_MESSAGE);
				else {
					if (data.replaceAll("-", "").trim().isEmpty())
						JOptionPane.showMessageDialog(null, "Data Vazia", "Data Vazia", JOptionPane.ERROR_MESSAGE);
					else {
						int dia, mes, ano;
						if (data.replaceAll("-", "").trim().length() != 8)
							JOptionPane.showMessageDialog(null, "Data Incompleta", "Data Incompleta",
									JOptionPane.ERROR_MESSAGE);
						else {
							dia = Integer.parseInt(data.substring(0, data.indexOf("-")).trim());
							if (dia <= 0 || dia >= 32)
								JOptionPane.showMessageDialog(null, "Dia Inv�lido", "Dia Inv�lido!",
										JOptionPane.ERROR_MESSAGE);
							else {
								mes = Integer
										.parseInt(data.substring(data.indexOf("-") + 1, data.lastIndexOf("-")).trim());
								if (mes <= 0 || mes >= 13)
									JOptionPane.showMessageDialog(null, "M�s Inv�lido", "M�s Inv�lido!",
											JOptionPane.ERROR_MESSAGE);
								else {
									ano = Integer
											.parseInt(data.substring(data.lastIndexOf("-") + 1, data.length()).trim());
									if (ano <= 1900 || ano > Calendar.getInstance().get(Calendar.YEAR))
										JOptionPane.showMessageDialog(null, "Ano Inv�lido", "Ano Inv�lido!",
												JOptionPane.ERROR_MESSAGE);
									else {
										if (Double.parseDouble(txt_valor_seguro.getValue().toString()) == 0)
											JOptionPane.showMessageDialog(null, "Valor da seguro n�o pode ser 0!",
													"Valor Inv�lido!", JOptionPane.ERROR_MESSAGE);
										else {
											try {
												rs = stmt.executeQuery("SELECT * FROM Seguro WHERE Matricula LIKE '"
														+ txt_matricula_seguro.getSelectedItem() + "'");
												if (rs.isBeforeFirst())
													JOptionPane.showMessageDialog(null,
															"Este carro com a matricula: '"
																	+ txt_matricula_seguro.getSelectedItem()
																	+ "' j� tem seguro",
															"Carro j� tem seguro!", JOptionPane.INFORMATION_MESSAGE);
												else {
													String insert = "INSERT INTO Seguro(Nome,DataValidade,Valor,Matricula)VALUES('"
															+ txt_nome_seguro.getText() + "','"
															+ txt_data_seguro.getText() + "',"
															+ txt_valor_seguro.getValue() + ",'"
															+ txt_matricula_seguro.getSelectedItem() + "')";
													stmt.executeUpdate(insert);
													JOptionPane.showMessageDialog(null,
															"Seguro adicionado com sucesso!", "Seguro Inserido",
															JOptionPane.INFORMATION_MESSAGE);
													txt_nome_seguro.setText("");
													txt_data_seguro.setText("");
													txt_valor_seguro.setValue(0);
												}
											} catch (SQLException se) {
												se.printStackTrace();
											}
										}
									}
								}
							}
						}
					}
				}
			}
		});
		btnInserirSeguro.setFocusPainted(false);
		btnInserirSeguro.setContentAreaFilled(false);
		btnInserirSeguro.setBounds(212, 139, 116, 23);
		panel_seguro.add(btnInserirSeguro);

		JSeparator separator = new JSeparator();
		separator.setBounds(0, 35, 509, 4);
		panel_adicioar.add(separator);

		panel_consultar = new JPanel();
		tabbedPane.addTab("Consultar e Editar Taxas", null, panel_consultar, null);
		panel_consultar.setLayout(null);
		
		JLabel lb_taxa_consultar = new JLabel("Tipo de Taxa :");
		lb_taxa_consultar.setBounds(290, 6, 90, 18);
		lb_taxa_consultar.setFont(new Font("Trebuchet MS", Font.PLAIN, 14));
		panel_consultar.add(lb_taxa_consultar);
		
		combo_taxa_consultar = new JComboBox<String>();
		combo_taxa_consultar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				try
				{
					String tabela = combo_taxa_consultar.getSelectedItem().toString();
					rs = stmt.executeQuery("SELECT * FROM " + tabela);
					table.setModel(Funcoes.buildTableModel(rs));
					if ( tabela.equals("Inspecao") )
					{
						combo_editar_matricula_inspecao.removeAllItems();
						txt_editar_data_inspecao.setText("");
						txt_editar_obs_inspecao.setText("");
						txt_editar_valor_inspecao.setValue(0);
						lb_editar_obs_inspecao_counter.setText("Letras usadas : 0/200!");
						bgEditar.clearSelection();
						panel_editar_inspecao.setVisible(true);
						panel_editar_iuc.setVisible(false);
						panel_editar_revisao.setVisible(false);
						panel_editar_seguro.setVisible(false);
					}
					else if ( tabela.equals("IUC") )
					{
						combo_editar_matricula_iuc.removeAllItems();
						txt_editar_data_iuc.setText("");
						txt_editar_valor_iuc.setValue(0);
						panel_editar_inspecao.setVisible(false);
						panel_editar_iuc.setVisible(true);
						panel_editar_revisao.setVisible(false);
						panel_editar_seguro.setVisible(false);
					}
					else if ( tabela.equals("Revisao") )
					{
						combo_editar_matricula_revisao.removeAllItems();
						txt_editar_oficina_revisao.setText("");
						txt_editar_kms_revisao.setValue(0);
						txt_editar_data_revisao.setText("");
						txt_editar_obs_revisao.setText("");
						txt_editar_valor_revisao.setValue(0);
						lb_editar_obs_revisao_counter.setText("Letras usadas : 0/200!");
						panel_editar_inspecao.setVisible(false);
						panel_editar_iuc.setVisible(false);
						panel_editar_revisao.setVisible(true);
						panel_editar_seguro.setVisible(false);
					}
					else if ( tabela.equals("Seguro") )
					{
						combo_editar_matricula_seguro.removeAllItems();
						txt_editar_nome_seguro.setText("");
						txt_editar_data_seguro.setText("");
						txt_editar_valor_seguro.setValue(0);
						panel_editar_inspecao.setVisible(false);
						panel_editar_iuc.setVisible(false);
						panel_editar_revisao.setVisible(false);
						panel_editar_seguro.setVisible(true);
					}
				}catch(Exception ex)
				{
					ex.printStackTrace();
				}
			}
		});
		combo_taxa_consultar.setBounds(390, 6, 117, 20);
		combo_taxa_consultar.setModel(new DefaultComboBoxModel(new String[] {"Inspecao", "IUC", "Revisao", "Seguro"}));
		panel_consultar.add(combo_taxa_consultar);
		
		try
		{
			rs = stmt.executeQuery("SELECT * FROM " + combo_taxa_consultar.getSelectedItem());
			table = new JTable(Funcoes.buildTableModel(rs)){
				public boolean isCellEditable(int rowIndex, int mColIndex)
				{
					return false;
				}
			};
			table.addMouseListener( new MouseAdapter(){
				@Override
				public void mousePressed(MouseEvent me) {
					JTable table =(JTable) me.getSource();
			        Point p = me.getPoint();
			        int row = table.rowAtPoint(p);
			        if (me.getClickCount() >= 1 && row != -1)
			        {
						try
						{
							String matricula = table.getValueAt(row, table.getColumn("Matricula").getModelIndex()).toString(),
								   tabela = combo_taxa_consultar.getSelectedItem().toString();
							
							rs = stmt.executeQuery("SELECT * FROM " + combo_taxa_consultar.getSelectedItem() + " WHERE Matricula LIKE '" + matricula + "'");
							rs.next();
							
							if ( tabela.equals("Inspecao") )
							{
								combo_editar_matricula_inspecao.removeAllItems();
								combo_editar_matricula_inspecao.addItem(matricula);
								txt_editar_data_inspecao.setText(rs.getString("DataInspec"));
								txt_editar_obs_inspecao.setText(rs.getString("Obs"));
								txt_editar_valor_inspecao.setValue(rs.getDouble("Valor"));
								lb_editar_obs_inspecao_counter.setText("Letras usadas : " + txt_editar_obs_inspecao.getText().length() + "/200!");
								if ( rs.getString("Passou").equals("Sim"))
									rdbEditarSimInspecao.setSelected(true);
								else
									rdbEditarNaoInspecao.setSelected(true);
							}
							else if ( tabela.equals("IUC") )
							{
								combo_editar_matricula_iuc.removeAllItems();
								combo_editar_matricula_iuc.addItem(matricula);
								txt_editar_data_iuc.setText(rs.getString("DataValidade"));
								txt_editar_valor_iuc.setValue(rs.getDouble("Valor"));
							}
							else if ( tabela.equals("Revisao") )
							{
								combo_editar_matricula_revisao.removeAllItems();
								combo_editar_matricula_revisao.addItem(matricula);
								txt_editar_oficina_revisao.setText(rs.getString("Oficina"));
								txt_editar_data_revisao.setText(rs.getString("DataRev"));
								txt_editar_kms_revisao.setValue(rs.getDouble("Kms"));
								txt_editar_valor_revisao.setValue(rs.getDouble("Valor"));
								txt_editar_obs_revisao.setText(rs.getString("Obs"));
								lb_editar_obs_revisao_counter.setText("Letras usadas : " + txt_editar_obs_revisao.getText().length() + "/200!");
							}
							else if ( tabela.equals("Seguro") )
							{
								combo_editar_matricula_seguro.removeAllItems();
								combo_editar_matricula_seguro.addItem(matricula);
								txt_editar_nome_seguro.setText(rs.getString("Nome"));
								txt_editar_data_seguro.setText(rs.getString("DataValidade"));
								txt_editar_valor_seguro.setValue(rs.getDouble("Valor"));
							}
						}
						catch (Exception e)
						{
							e.printStackTrace();
						}
			        }
				}
			});
		}catch(Exception ex)
		{
			ex.printStackTrace();
		}
		
		
		JScrollPane scrollPaneEditarTabela = new JScrollPane(table);
		scrollPaneEditarTabela.setBounds(0, 35, 839, 212);
		panel_consultar.add(scrollPaneEditarTabela, BorderLayout.CENTER);
		
		JLabel lb_como_editar = new JLabel("Clique numa linha da tabela para editar!");
		lb_como_editar.setHorizontalAlignment(SwingConstants.LEFT);
		lb_como_editar.setForeground(Color.BLUE);
		lb_como_editar.setFont(new Font("Trebuchet MS", Font.PLAIN, 12));
		lb_como_editar.setBounds(298, 248, 221, 15);
		
		panel_consultar.add(lb_como_editar);
		
		panel_editar_inspecao = new JPanel();
		panel_editar_inspecao.setBounds(0, 266, 839, 198);
		panel_consultar.add(panel_editar_inspecao);
		panel_editar_inspecao.setLayout(null);
		
		lb_editar_matricula_inspecao = new JLabel("<html> Matr\u00EDcula : <font color='red'>*</font></html>");
		lb_editar_matricula_inspecao.setBounds(60, 8, 76, 18);
		lb_editar_matricula_inspecao.setFont(new Font("Trebuchet MS", Font.PLAIN, 14));
		panel_editar_inspecao.add(lb_editar_matricula_inspecao);
		
		lb_editar_data_inspecao = new JLabel("<html> Data de Inspe\u00E7\u00E3o : <font color='red'>*</font></html>");
		lb_editar_data_inspecao.setBounds(9, 37, 127, 18);
		lb_editar_data_inspecao.setFont(new Font("Trebuchet MS", Font.PLAIN, 14));
		panel_editar_inspecao.add(lb_editar_data_inspecao);
		
		txt_editar_data_inspecao = new JFormattedTextField();
		txt_editar_data_inspecao.setBounds(146, 34, 116, 24);
		txt_editar_data_inspecao.setToolTipText("dd/MM/yyyy");
		txt_editar_data_inspecao.setHorizontalAlignment(SwingConstants.RIGHT);
		txt_editar_data_inspecao.setFont(new Font("Trebuchet MS", Font.PLAIN, 14));
		txt_editar_data_inspecao.setColumns(10);
		Funcoes.adicionarMask("##-##-####", txt_editar_data_inspecao);
		panel_editar_inspecao.add(txt_editar_data_inspecao);
		
		lb_editar_obs_inspecao = new JLabel("<html> Observa\u00E7\u00F5es : <font color='red'>*</font></html>");
		lb_editar_obs_inspecao.setBounds(39, 67, 97, 18);
		lb_editar_obs_inspecao.setFont(new Font("Trebuchet MS", Font.PLAIN, 14));
		panel_editar_inspecao.add(lb_editar_obs_inspecao);
		
		lb_editar_valor = new JLabel("<html> Valor : <font color='red'>*</font></html>");
		lb_editar_valor.setBounds(445, 8, 50, 18);
		lb_editar_valor.setFont(new Font("Trebuchet MS", Font.PLAIN, 14));
		panel_editar_inspecao.add(lb_editar_valor);
		
		txt_editar_valor_inspecao = new JSpinner(new SpinnerNumberModel(0.00, 0.00, 1000.00, 10.00));
		txt_editar_valor_inspecao.setBounds(505, 6, 107, 22);
		txt_editar_valor_inspecao.setFont(new Font("Trebuchet MS", Font.PLAIN, 12));
		modificarSpinners(txt_editar_valor_inspecao);
		panel_editar_inspecao.add(txt_editar_valor_inspecao);
		
		lb_editar_passou = new JLabel("<html> Passou : <font color='red'>*</font></html>");
		lb_editar_passou.setBounds(434, 37, 61, 18);
		lb_editar_passou.setFont(new Font("Trebuchet MS", Font.PLAIN, 14));
		panel_editar_inspecao.add(lb_editar_passou);
		
		rdbEditarSimInspecao = new JRadioButton("Sim");
		rdbEditarSimInspecao.setBounds(505, 33, 47, 25);
		rdbEditarSimInspecao.setFont(new Font("Tahoma", Font.PLAIN, 14));
		bgEditar.add(rdbEditarSimInspecao);
		panel_editar_inspecao.add(rdbEditarSimInspecao);
		
		rdbEditarNaoInspecao = new JRadioButton("N\u00E3o");
		rdbEditarNaoInspecao.setBounds(563, 33, 49, 25);
		rdbEditarNaoInspecao.setFont(new Font("Tahoma", Font.PLAIN, 14));
		bgEditar.add(rdbEditarNaoInspecao);
		panel_editar_inspecao.add(rdbEditarNaoInspecao);
		
		combo_editar_matricula_inspecao = new JComboBox<String>();
		combo_editar_matricula_inspecao.setBounds(146, 8, 116, 20);
		panel_editar_inspecao.add(combo_editar_matricula_inspecao);
		
		lb_editar_obs_inspecao_counter = new JLabel("Letras usadas : 0/200!");
		lb_editar_obs_inspecao_counter.setBounds(479, 171, 133, 15);
		lb_editar_obs_inspecao_counter.setHorizontalAlignment(SwingConstants.LEFT);
		lb_editar_obs_inspecao_counter.setForeground(Color.GREEN);
		lb_editar_obs_inspecao_counter.setFont(new Font("Trebuchet MS", Font.PLAIN, 12));
		panel_editar_inspecao.add(lb_editar_obs_inspecao_counter);
		
		txt_editar_obs_inspecao = new JTextArea();
		txt_editar_obs_inspecao.addKeyListener(new KeyAdapter() {
			@Override
			public void keyTyped(KeyEvent e) {
				if (txt_editar_obs_inspecao.getText().length() <= 200)
					lb_editar_obs_inspecao_counter.setForeground(Color.green);
				else
					lb_editar_obs_inspecao_counter.setForeground(Color.red);
				lb_editar_obs_inspecao_counter.setText("Letras usadas : " + txt_editar_obs_inspecao.getText().length() + "/200!");
			}
		});
		txt_editar_obs_inspecao.setLineWrap(true);
		txt_editar_obs_inspecao.setFont(new Font("Trebuchet MS", Font.PLAIN, 14));
		
		btnEditarInspecao = new JButton("Editar Inspe\u00E7\u00E3o");
		btnEditarInspecao.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				if ( table.getSelectedRow() != -1)
				{
					String data = txt_editar_data_inspecao.getText();
					if (data.replaceAll("-", "").trim().isEmpty())
						JOptionPane.showMessageDialog(null, "Data Vazia", "Data Vazia", JOptionPane.ERROR_MESSAGE);
					else
					{
						int dia, mes, ano;
						if (data.replaceAll("-", "").trim().length() != 8)
							JOptionPane.showMessageDialog(null, "Data Incompleta", "Data Incompleta", JOptionPane.ERROR_MESSAGE);
						else
						{
							dia = Integer.parseInt(data.substring(0, data.indexOf("-")).trim());
							if (dia <= 0 || dia >= 32)
								JOptionPane.showMessageDialog(null, "Dia Inv�lido", "Dia Inv�lido!", JOptionPane.ERROR_MESSAGE);
							else
							{
								mes = Integer.parseInt(data.substring(data.indexOf("-") + 1, data.lastIndexOf("-")).trim());
								if (mes <= 0 || mes >= 13)
									JOptionPane.showMessageDialog(null, "M�s Inv�lido", "M�s Inv�lido!", JOptionPane.ERROR_MESSAGE);
								else
								{
									ano = Integer.parseInt(data.substring(data.lastIndexOf("-") + 1, data.length()).trim());
									if (ano <= 1900 || ano > Calendar.getInstance().get(Calendar.YEAR))
										JOptionPane.showMessageDialog(null, "Ano Inv�lido", "Ano Inv�lido!",JOptionPane.ERROR_MESSAGE);
									else
									{
										if (txt_editar_obs_inspecao.getText().length() > 2000)
											JOptionPane.showMessageDialog(null, "Observa��o demasiado grande","Observa��o Inv�lida!", JOptionPane.ERROR_MESSAGE);
										else if (txt_editar_obs_inspecao.getText().isEmpty())
											JOptionPane.showMessageDialog(null, "Observa��o vazia","Observa��o Inv�lida!", JOptionPane.ERROR_MESSAGE);
										else
										{
											if (Double.parseDouble(txt_editar_valor_inspecao.getValue().toString()) == 0)
												JOptionPane.showMessageDialog(null,"Valor da inspe��o n�o pode ser 0!", "Valor Inv�lido!",JOptionPane.ERROR_MESSAGE);
											else
											{
												int result = JOptionPane.showConfirmDialog(null, "Deseja editar a inspe��o da matricula '" + combo_editar_matricula_inspecao.getSelectedItem()
												+ "'?", "Editar Inspe��o", JOptionPane.YES_NO_OPTION, JOptionPane.QUESTION_MESSAGE);
												if(result == JOptionPane.YES_OPTION)
												{
													try
													{
														String passou = "", id = table.getValueAt(table.getSelectedRow(), table.getColumn("IdInspecao").getModelIndex()).toString();
														if (rdbEditarSimInspecao.isSelected())
															passou = "Sim";
														else if (rdbEditarNaoInspecao.isSelected())
															passou = "N�o";
														
														String alterar = "UPDATE Inspecao SET " +
																"DataInspec='" + txt_editar_data_inspecao.getText() +
																"',Obs='" + txt_editar_obs_inspecao.getText() +
																"',Valor=" + txt_editar_valor_inspecao.getValue() +
																",Passou='" +  passou + "' WHERE IdInspecao=" + id;
														stmt.executeUpdate(alterar);
														JOptionPane.showMessageDialog(null, "Inspe��o editada com sucesso!", "Inspe��o Editada", JOptionPane.INFORMATION_MESSAGE);
														combo_editar_matricula_inspecao.removeAllItems();
														txt_editar_data_inspecao.setText("");
														txt_editar_obs_inspecao.setText("");
														txt_editar_valor_inspecao.setValue(0);
														lb_editar_obs_inspecao_counter.setText("Letras usadas : 0/200!");
														bgEditar.clearSelection();
														rs = stmt.executeQuery("SELECT * FROM Inspecao");
														table.setModel(Funcoes.buildTableModel(rs));
													}
													catch(SQLException sqle)
													{
														sqle.printStackTrace();
													}
												}
											}
										}
									}
								}
							}
						}
					}
				}
				else
					JOptionPane.showMessageDialog(null, "Escolha uma inspe��o para editar!", "Inspe��o Inv�lida", JOptionPane.ERROR_MESSAGE);
			}
		});
		btnEditarInspecao.setBounds(642, 8, 150, 50);
		btnEditarInspecao.setFocusPainted(false);
		btnEditarInspecao.setContentAreaFilled(false);
		panel_editar_inspecao.add(btnEditarInspecao);
		
		btnEliminarInspecao = new JButton("Eliminar Inspe\u00E7\u00E3o");
		btnEliminarInspecao.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				if (table.getSelectedRow() != -1)
				{
					int result = JOptionPane.showConfirmDialog(null, "Deseja eliminar a inspe��o da matricula '" + combo_editar_matricula_inspecao.getSelectedItem()
							+ "'?", "Eliminar Inspe��o", JOptionPane.YES_NO_OPTION,JOptionPane.QUESTION_MESSAGE);
					if(result == JOptionPane.YES_OPTION)
					{
						try
						{
							String id = table.getValueAt(table.getSelectedRow(), table.getColumn("IdInspecao").getModelIndex()).toString();
							String query = "DELETE FROM Inspecao WHERE IdInspecao=" + id;
							stmt.close();
							rs.close();
							stmt.executeUpdate(query);
							JOptionPane.showMessageDialog(null, "Inspe��o eliminada com sucesso!", "Inspe��o Eliminada", JOptionPane.INFORMATION_MESSAGE);
							combo_editar_matricula_inspecao.removeAllItems();
							txt_editar_data_inspecao.setText("");
							txt_editar_obs_inspecao.setText("");
							txt_editar_valor_inspecao.setValue(0);
							lb_editar_obs_inspecao_counter.setText("Letras usadas : 0/200!");
							bgEditar.clearSelection();
							rs = stmt.executeQuery("SELECT * FROM Inspecao");
							table.setModel(Funcoes.buildTableModel(rs));
						}
						catch(SQLException sqle)
						{
							sqle.printStackTrace();
						}
					}
				}
				else
					JOptionPane.showMessageDialog(null, "Escolha uma inspe��o para eliminar", "Inspe��o Inv�lida", JOptionPane.ERROR_MESSAGE);
			}
		});
		btnEliminarInspecao.setFocusPainted(false);
		btnEliminarInspecao.setContentAreaFilled(false);
		btnEliminarInspecao.setBounds(642, 117, 150, 50);
		panel_editar_inspecao.add(btnEliminarInspecao);
		
		scrollPaneEditarInspecao = new JScrollPane(txt_editar_obs_inspecao);
		scrollPaneEditarInspecao.setBounds(146, 69, 466, 98);
		panel_editar_inspecao.add(scrollPaneEditarInspecao);
		
		panel_editar_iuc = new JPanel();
		panel_editar_iuc.setBounds(0, 266, 839, 198);
		panel_consultar.add(panel_editar_iuc);
		panel_editar_iuc.setLayout(null);
		
		lb_editar_datavalidade_iuc = new JLabel("<html> Data de Validade : <font color='red'>*</font></html>");
		lb_editar_datavalidade_iuc.setBounds(269, 48, 127, 18);
		lb_editar_datavalidade_iuc.setFont(new Font("Trebuchet MS", Font.PLAIN, 14));
		panel_editar_iuc.add(lb_editar_datavalidade_iuc);
		
		txt_editar_data_iuc = new JFormattedTextField();
		txt_editar_data_iuc.setBounds(406, 45, 116, 24);
		txt_editar_data_iuc.setToolTipText("dd/MM/yyyy");
		txt_editar_data_iuc.setHorizontalAlignment(SwingConstants.RIGHT);
		txt_editar_data_iuc.setFont(new Font("Trebuchet MS", Font.PLAIN, 14));
		txt_editar_data_iuc.setColumns(10);
		Funcoes.adicionarMask("##-##-####", txt_editar_data_iuc);
		panel_editar_iuc.add(txt_editar_data_iuc);
		
		lb_editar_matricula_iuc = new JLabel("<html> Matr\u00EDcula : <font color='red'>*</font></html>");
		lb_editar_matricula_iuc.setBounds(320, 7, 76, 18);
		lb_editar_matricula_iuc.setFont(new Font("Trebuchet MS", Font.PLAIN, 14));
		panel_editar_iuc.add(lb_editar_matricula_iuc);
		
		combo_editar_matricula_iuc = new JComboBox<String>();
		combo_editar_matricula_iuc.setBounds(406, 7, 116, 20);
		panel_editar_iuc.add(combo_editar_matricula_iuc);
		
		lb_editar_valor_iuc = new JLabel("<html> Valor : <font color='red'>*</font></html>");
		lb_editar_valor_iuc.setBounds(346, 89, 50, 18);
		lb_editar_valor_iuc.setFont(new Font("Trebuchet MS", Font.PLAIN, 14));
		panel_editar_iuc.add(lb_editar_valor_iuc);
		
		txt_editar_valor_iuc = new JSpinner(new SpinnerNumberModel(0.00, 0.00, 1000.00, 10.00));
		txt_editar_valor_iuc.setBounds(406, 88, 116, 22);
		txt_editar_valor_iuc.setFont(new Font("Trebuchet MS", Font.PLAIN, 12));
		modificarSpinners(txt_editar_valor_iuc);
		panel_editar_iuc.add(txt_editar_valor_iuc);
		
		btnEditarIUC = new JButton("Editar IUC");
		btnEditarIUC.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				if ( table.getSelectedRow() != -1)
				{
					String data = txt_editar_data_iuc.getText();
					if (data.replaceAll("-", "").trim().isEmpty())
						JOptionPane.showMessageDialog(null, "Data Vazia", "Data Vazia", JOptionPane.ERROR_MESSAGE);
					else
					{
						int dia, mes, ano;
						if (data.replaceAll("-", "").trim().length() != 8)
							JOptionPane.showMessageDialog(null, "Data Incompleta", "Data Incompleta", JOptionPane.ERROR_MESSAGE);
						else
						{
							dia = Integer.parseInt(data.substring(0, data.indexOf("-")).trim());
							if (dia <= 0 || dia >= 32)
								JOptionPane.showMessageDialog(null, "Dia Inv�lido", "Dia Inv�lido!", JOptionPane.ERROR_MESSAGE);
							else
							{
								mes = Integer.parseInt(data.substring(data.indexOf("-") + 1, data.lastIndexOf("-")).trim());
								if (mes <= 0 || mes >= 13)
									JOptionPane.showMessageDialog(null, "M�s Inv�lido", "M�s Inv�lido!", JOptionPane.ERROR_MESSAGE);
								else
								{
									ano = Integer.parseInt(data.substring(data.lastIndexOf("-") + 1, data.length()).trim());
									if (ano <= 1900 || ano > Calendar.getInstance().get(Calendar.YEAR))
										JOptionPane.showMessageDialog(null, "Ano Inv�lido", "Ano Inv�lido!",JOptionPane.ERROR_MESSAGE);
									else
									{
										if (Double.parseDouble(txt_editar_valor_iuc.getValue().toString()) == 0)
											JOptionPane.showMessageDialog(null,"Valor do IUC n�o pode ser 0!", "Valor Inv�lido!",JOptionPane.ERROR_MESSAGE);
										else
										{
											int result = JOptionPane.showConfirmDialog(null, "Deseja editar o IUC da matricula '" + combo_editar_matricula_iuc.getSelectedItem()
											+ "'?", "Editar IUC", JOptionPane.YES_NO_OPTION, JOptionPane.QUESTION_MESSAGE);
											if(result == JOptionPane.YES_OPTION)
											{
												try
												{
													String id = table.getValueAt(table.getSelectedRow(), table.getColumn("IdIuc").getModelIndex()).toString();
													String alterar = "UPDATE IUC SET " +
															"DataValidade='" + txt_editar_data_iuc.getText() +
															"',Valor=" + txt_editar_valor_iuc.getValue() +
															" WHERE IdIuc=" + id;
													stmt.executeUpdate(alterar);
													JOptionPane.showMessageDialog(null, "IUC editada com sucesso!", "IUC Editada", JOptionPane.INFORMATION_MESSAGE);
													combo_editar_matricula_iuc.removeAllItems();
													txt_editar_data_iuc.setText("");
													txt_editar_valor_iuc.setValue(0);
													rs = stmt.executeQuery("SELECT * FROM IUC");
													table.setModel(Funcoes.buildTableModel(rs));
												}
												catch(SQLException sqle)
												{
													sqle.printStackTrace();
												}
											}
										}
									}
								}
							}
						}
					}
				}
				else
					JOptionPane.showMessageDialog(null, "Escolha uma inspe��o para editar!", "Inspe��o Inv�lida", JOptionPane.ERROR_MESSAGE);
			}
		});
		btnEditarIUC.setFocusPainted(false);
		btnEditarIUC.setContentAreaFilled(false);
		btnEditarIUC.setBounds(406, 120, 116, 32);
		panel_editar_iuc.add(btnEditarIUC);
		
		btnEliminarIUC = new JButton("Eliminar IUC");
		btnEliminarIUC.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				if (table.getSelectedRow() != -1)
				{
					int result = JOptionPane.showConfirmDialog(null, "Deseja eliminar o IUC da matricula '" + combo_editar_matricula_iuc.getSelectedItem()
							+ "'?", "Eliminar IUC", JOptionPane.YES_NO_OPTION,JOptionPane.QUESTION_MESSAGE);
					if(result == JOptionPane.YES_OPTION)
					{
						try
						{
							String id = table.getValueAt(table.getSelectedRow(), table.getColumn("IdIuc").getModelIndex()).toString();
							String query = "DELETE FROM IUC WHERE IdIuc=" + id;
							stmt.executeUpdate(query);
							JOptionPane.showMessageDialog(null, "IUC eliminado com sucesso!", "IUC Eliminada", JOptionPane.INFORMATION_MESSAGE);
							combo_editar_matricula_iuc.removeAllItems();
							txt_editar_data_iuc.setText("");
							txt_editar_valor_iuc.setValue(0);
							rs = stmt.executeQuery("SELECT * FROM IUC");
							table.setModel(Funcoes.buildTableModel(rs));
						}
						catch(SQLException sqle)
						{
							sqle.printStackTrace();
						}
					}
				}
				else
					JOptionPane.showMessageDialog(null, "Escolha um IUC para eliminar", "IUC Inv�lido", JOptionPane.ERROR_MESSAGE);
			}
		});
		btnEliminarIUC.setFocusPainted(false);
		btnEliminarIUC.setContentAreaFilled(false);
		btnEliminarIUC.setBounds(406, 155, 116, 32);
		panel_editar_iuc.add(btnEliminarIUC);
		
		panel_editar_revisao = new JPanel();
		panel_editar_revisao.setBounds(0, 266, 839, 198);
		panel_consultar.add(panel_editar_revisao);
		panel_editar_revisao.setLayout(null);
		
		lb_editar_matricula_revisao = new JLabel("<html> Matr\u00EDcula : <font color='red'>*</font></html>");
		lb_editar_matricula_revisao.setFont(new Font("Trebuchet MS", Font.PLAIN, 14));
		lb_editar_matricula_revisao.setBounds(61, 13, 76, 18);
		panel_editar_revisao.add(lb_editar_matricula_revisao);
		
		combo_editar_matricula_revisao = new JComboBox<String>();
		combo_editar_matricula_revisao.setBounds(147, 13, 116, 20);
		panel_editar_revisao.add(combo_editar_matricula_revisao);
		
		lb_editar_datarevisao = new JLabel("<html> Data de Revis\u00E3o : <font color='red'>*</font></html>");
		lb_editar_datarevisao.setFont(new Font("Trebuchet MS", Font.PLAIN, 14));
		lb_editar_datarevisao.setBounds(19, 42, 119, 18);
		panel_editar_revisao.add(lb_editar_datarevisao);
		
		txt_editar_data_revisao = new JFormattedTextField();
		txt_editar_data_revisao.setToolTipText("dd/MM/yyyy");
		txt_editar_data_revisao.setHorizontalAlignment(SwingConstants.RIGHT);
		txt_editar_data_revisao.setFont(new Font("Trebuchet MS", Font.PLAIN, 14));
		txt_editar_data_revisao.setColumns(10);
		txt_editar_data_revisao.setBounds(147, 39, 116, 24);
		Funcoes.adicionarMask("##-##-####", txt_editar_data_revisao);
		panel_editar_revisao.add(txt_editar_data_revisao);
		
		lb_editar_obs_revisao = new JLabel("<html> Observa\u00E7\u00F5es : <font color='red'>*</font></html>");
		lb_editar_obs_revisao.setFont(new Font("Trebuchet MS", Font.PLAIN, 14));
		lb_editar_obs_revisao.setBounds(40, 72, 97, 18);
		panel_editar_revisao.add(lb_editar_obs_revisao);
		
		txt_editar_obs_revisao = new JTextArea();
		txt_editar_obs_revisao.setLineWrap(true);
		txt_editar_obs_revisao.setFont(new Font("Trebuchet MS", Font.PLAIN, 14));
		txt_editar_obs_revisao.setBounds(148, 75, 464, 96);
		panel_editar_revisao.add(txt_editar_obs_revisao);
		
		lb_editar_valor_revisao = new JLabel("<html> Valor : <font color='red'>*</font></html>");
		lb_editar_valor_revisao.setFont(new Font("Trebuchet MS", Font.PLAIN, 14));
		lb_editar_valor_revisao.setBounds(446, 14, 50, 18);
		panel_editar_revisao.add(lb_editar_valor_revisao);
		
		txt_editar_valor_revisao = new JSpinner(new SpinnerNumberModel(0.00, 0.00, 1000.00, 10.00));
		txt_editar_valor_revisao.setFont(new Font("Trebuchet MS", Font.PLAIN, 12));
		txt_editar_valor_revisao.setBounds(506, 12, 107, 22);
		modificarSpinners(txt_editar_valor_revisao);
		panel_editar_revisao.add(txt_editar_valor_revisao);
		
		btnEditarRevisao = new JButton("Editar Revis\u00E3o");
		btnEditarRevisao.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				if ( table.getSelectedRow() != -1)
				{
					String data = txt_editar_data_revisao.getText();
					if ( txt_editar_oficina_revisao.getText().isEmpty() )
						JOptionPane.showMessageDialog(null, "Oficina est� vazia", "Oficina Vazia", JOptionPane.ERROR_MESSAGE);
					else
					{
						if (data.replaceAll("-", "").trim().isEmpty())
							JOptionPane.showMessageDialog(null, "Data Vazia", "Data Vazia", JOptionPane.ERROR_MESSAGE);
						else
						{
							int dia, mes, ano;
							if (data.replaceAll("-", "").trim().length() != 8)
								JOptionPane.showMessageDialog(null, "Data Incompleta", "Data Incompleta", JOptionPane.ERROR_MESSAGE);
							else
							{
								dia = Integer.parseInt(data.substring(0, data.indexOf("-")).trim());
								if (dia <= 0 || dia >= 32)
									JOptionPane.showMessageDialog(null, "Dia Inv�lido", "Dia Inv�lido!", JOptionPane.ERROR_MESSAGE);
								else
								{
									mes = Integer.parseInt(data.substring(data.indexOf("-") + 1, data.lastIndexOf("-")).trim());
									if (mes <= 0 || mes >= 13)
										JOptionPane.showMessageDialog(null, "M�s Inv�lido", "M�s Inv�lido!", JOptionPane.ERROR_MESSAGE);
									else
									{
										ano = Integer.parseInt(data.substring(data.lastIndexOf("-") + 1, data.length()).trim());
										if (ano <= 1900 || ano > Calendar.getInstance().get(Calendar.YEAR))
											JOptionPane.showMessageDialog(null, "Ano Inv�lido", "Ano Inv�lido!",JOptionPane.ERROR_MESSAGE);
										else
										{
											if (txt_editar_obs_revisao.getText().length() > 2000)
												JOptionPane.showMessageDialog(null, "Observa��o demasiado grande","Observa��o Inv�lida!", JOptionPane.ERROR_MESSAGE);
											else if (txt_editar_obs_revisao.getText().isEmpty())
												JOptionPane.showMessageDialog(null, "Observa��o vazia","Observa��o Inv�lida!", JOptionPane.ERROR_MESSAGE);
											else
											{
												if (Double.parseDouble(txt_editar_valor_revisao.getValue().toString()) == 0)
													JOptionPane.showMessageDialog(null,"Valor da revisao n�o pode ser 0!", "Valor Inv�lido!",JOptionPane.ERROR_MESSAGE);
												else
												{
													if (Double.parseDouble(txt_editar_kms_revisao.getValue().toString()) == 0)
														JOptionPane.showMessageDialog(null,"KMs da revisao n�o pode ser 0!", "Numero de KMs est� Inv�lido!",JOptionPane.ERROR_MESSAGE);
													else
													{
														int result = JOptionPane.showConfirmDialog(null, "Deseja editar a revis�o da matricula '" + combo_editar_matricula_revisao.getSelectedItem()
														+ "'?", "Editar Revis�o", JOptionPane.YES_NO_OPTION, JOptionPane.QUESTION_MESSAGE);
														if(result == JOptionPane.YES_OPTION)
														{
															try
															{
																String id = table.getValueAt(table.getSelectedRow(), table.getColumn("IdRevisao").getModelIndex()).toString();
																String alterar = "UPDATE Revisao SET " +
																		"Oficina='" + txt_editar_oficina_revisao.getText() +
																		"',Kms=" + txt_editar_kms_revisao.getValue() +
																		",DataRev='" + txt_editar_data_revisao.getText() +
																		"',Obs='" + txt_editar_obs_revisao.getText() +
																		"',Valor=" + txt_editar_valor_revisao.getValue() +
																		" WHERE IdRevisao=" + id;
																stmt.executeUpdate(alterar);
																JOptionPane.showMessageDialog(null, "Revis�o editada com sucesso!", "Revis�o Editada", JOptionPane.INFORMATION_MESSAGE);
																combo_editar_matricula_revisao.removeAllItems();
																txt_editar_data_revisao.setText("");
																txt_editar_oficina_revisao.setText("");
																txt_editar_obs_revisao.setText("");
																txt_editar_valor_revisao.setValue(0);
																txt_editar_kms_revisao.setValue(0);
																lb_editar_obs_revisao_counter.setText("Letras usadas : 0/200!");
																rs = stmt.executeQuery("SELECT * FROM Revisao");
																table.setModel(Funcoes.buildTableModel(rs));
															}
															catch(SQLException sqle)
															{
																sqle.printStackTrace();
															}
														}
													}
												}
											}
										}
									}
								}
							}
						}
					}
				}
				else
					JOptionPane.showMessageDialog(null, "Escolha uma revis�o para editar!", "Revis�o Inv�lida", JOptionPane.ERROR_MESSAGE);
			}
		});
		btnEditarRevisao.setFocusPainted(false);
		btnEditarRevisao.setContentAreaFilled(false);
		btnEditarRevisao.setBounds(643, 13, 150, 50);
		panel_editar_revisao.add(btnEditarRevisao);
		
		btnEliminarRevisao = new JButton("Eliminar Revis\u00E3o");
		btnEliminarRevisao.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				if (table.getSelectedRow() != -1)
				{
					int result = JOptionPane.showConfirmDialog(null, "Deseja eliminar a revis�o da matricula '" + combo_editar_matricula_revisao.getSelectedItem()
							+ "'?", "Eliminar Revis�o", JOptionPane.YES_NO_OPTION,JOptionPane.QUESTION_MESSAGE);
					if(result == JOptionPane.YES_OPTION)
					{
						try
						{
							String id = table.getValueAt(table.getSelectedRow(), table.getColumn("IdRevisao").getModelIndex()).toString();
							String query = "DELETE FROM Revisao WHERE IdRevisao=" + id;
							stmt.executeUpdate(query);
							JOptionPane.showMessageDialog(null, "Revis�o eliminada com sucesso!", "Revis�o Eliminada", JOptionPane.INFORMATION_MESSAGE);
							combo_editar_matricula_revisao.removeAllItems();
							txt_editar_data_revisao.setText("");
							txt_editar_obs_revisao.setText("");
							txt_editar_oficina_revisao.setText("");
							txt_editar_kms_revisao.setValue(0);
							txt_editar_valor_revisao.setValue(0);
							lb_editar_obs_revisao_counter.setText("Letras usadas : 0/200!");
							rs = stmt.executeQuery("SELECT * FROM Revisao");
							table.setModel(Funcoes.buildTableModel(rs));
						}
						catch(SQLException sqle)
						{
							sqle.printStackTrace();
						}
					}
				}
				else
					JOptionPane.showMessageDialog(null, "Escolha uma revis�o para eliminar", "Revis�o Inv�lida", JOptionPane.ERROR_MESSAGE);
			}
		});
		btnEliminarRevisao.setFocusPainted(false);
		btnEliminarRevisao.setContentAreaFilled(false);
		btnEliminarRevisao.setBounds(643, 122, 150, 50);
		panel_editar_revisao.add(btnEliminarRevisao);
		
		lb_editar_obs_revisao_counter = new JLabel("Letras usadas : 0/200!");
		lb_editar_obs_revisao_counter.setHorizontalAlignment(SwingConstants.LEFT);
		lb_editar_obs_revisao_counter.setForeground(Color.GREEN);
		lb_editar_obs_revisao_counter.setFont(new Font("Trebuchet MS", Font.PLAIN, 12));
		lb_editar_obs_revisao_counter.setBounds(480, 176, 133, 15);
		panel_editar_revisao.add(lb_editar_obs_revisao_counter);
		
		lb_editar_oficina_revisao = new JLabel("<html> Oficina : <font color='red'>*</font></html>");
		lb_editar_oficina_revisao.setFont(new Font("Trebuchet MS", Font.PLAIN, 14));
		lb_editar_oficina_revisao.setBounds(273, 42, 64, 18);
		panel_editar_revisao.add(lb_editar_oficina_revisao);
		
		lb_editar_kms_revisao = new JLabel("<html> KMs : <font color='red'>*</font></html>");
		lb_editar_kms_revisao.setFont(new Font("Trebuchet MS", Font.PLAIN, 14));
		lb_editar_kms_revisao.setBounds(273, 14, 43, 18);
		panel_editar_revisao.add(lb_editar_kms_revisao);
		
		txt_editar_kms_revisao = new JSpinner(new SpinnerNumberModel(0.00, 0.00, 9999999.00, 100.00));
		txt_editar_kms_revisao.setFont(new Font("Trebuchet MS", Font.PLAIN, 12));
		txt_editar_kms_revisao.setBounds(326, 12, 107, 22);
		modificarSpinners(txt_editar_kms_revisao);
		panel_editar_revisao.add(txt_editar_kms_revisao);
		
		
		txt_editar_oficina_revisao = new JTextField();
		txt_editar_oficina_revisao.setBounds(347, 42, 265, 20);
		panel_editar_revisao.add(txt_editar_oficina_revisao);
		txt_editar_oficina_revisao.setColumns(10);
		
		panel_editar_seguro = new JPanel();
		panel_editar_seguro.setBounds(0, 266, 839, 198);
		panel_consultar.add(panel_editar_seguro);
		panel_editar_seguro.setLayout(null);
		
		lb_editar_matricula_seguro = new JLabel("<html> Matr\u00EDcula : <font color='red'>*</font></html>");
		lb_editar_matricula_seguro.setFont(new Font("Trebuchet MS", Font.PLAIN, 14));
		lb_editar_matricula_seguro.setBounds(187, 42, 76, 18);
		panel_editar_seguro.add(lb_editar_matricula_seguro);
		
		combo_editar_matricula_seguro = new JComboBox<String>();
		combo_editar_matricula_seguro.setBounds(273, 42, 116, 20);
		panel_editar_seguro.add(combo_editar_matricula_seguro);
		
		lb_editar_data_seguro = new JLabel("<html> Data de Validade : <font color='red'>*</font></html>");
		lb_editar_data_seguro.setFont(new Font("Trebuchet MS", Font.PLAIN, 14));
		lb_editar_data_seguro.setBounds(136, 76, 127, 18);
		panel_editar_seguro.add(lb_editar_data_seguro);
		
		txt_editar_data_seguro = new JFormattedTextField();
		txt_editar_data_seguro.setToolTipText("dd/MM/yyyy");
		txt_editar_data_seguro.setHorizontalAlignment(SwingConstants.RIGHT);
		txt_editar_data_seguro.setFont(new Font("Trebuchet MS", Font.PLAIN, 14));
		txt_editar_data_seguro.setColumns(10);
		txt_editar_data_seguro.setBounds(273, 73, 116, 24);
		Funcoes.adicionarMask("##-##-####", txt_editar_data_seguro);
		panel_editar_seguro.add(txt_editar_data_seguro);
		
		label = new JLabel("<html> Valor : <font color='red'>*</font></html>");
		label.setFont(new Font("Trebuchet MS", Font.PLAIN, 14));
		label.setBounds(399, 77, 50, 18);
		panel_editar_seguro.add(label);
		
		txt_editar_valor_seguro = new JSpinner(new SpinnerNumberModel(0.00, 0.00, 1000.00, 10.00));
		txt_editar_valor_seguro.setFont(new Font("Trebuchet MS", Font.PLAIN, 14));
		txt_editar_valor_seguro.setBounds(459, 73, 127, 22);
		modificarSpinners(txt_editar_valor_seguro);
		panel_editar_seguro.add(txt_editar_valor_seguro);
		
		btnEditarSeugo = new JButton("Editar Seguro");
		btnEditarSeugo.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				if ( table.getSelectedRow() != -1)
				{
					if ( txt_editar_nome_seguro.getText().isEmpty() )
						JOptionPane.showMessageDialog(null, "Nome est� vazio", "Nome Vazio", JOptionPane.ERROR_MESSAGE);
					else
					{
						String data = txt_editar_data_seguro.getText();
						if (data.replaceAll("-", "").trim().isEmpty())
							JOptionPane.showMessageDialog(null, "Data Vazia", "Data Vazia", JOptionPane.ERROR_MESSAGE);
						else
						{
							int dia, mes, ano;
							if (data.replaceAll("-", "").trim().length() != 8)
								JOptionPane.showMessageDialog(null, "Data Incompleta", "Data Incompleta", JOptionPane.ERROR_MESSAGE);
							else
							{
								dia = Integer.parseInt(data.substring(0, data.indexOf("-")).trim());
								if (dia <= 0 || dia >= 32)
									JOptionPane.showMessageDialog(null, "Dia Inv�lido", "Dia Inv�lido!", JOptionPane.ERROR_MESSAGE);
								else
								{
									mes = Integer.parseInt(data.substring(data.indexOf("-") + 1, data.lastIndexOf("-")).trim());
									if (mes <= 0 || mes >= 13)
										JOptionPane.showMessageDialog(null, "M�s Inv�lido", "M�s Inv�lido!", JOptionPane.ERROR_MESSAGE);
									else
									{
										ano = Integer.parseInt(data.substring(data.lastIndexOf("-") + 1, data.length()).trim());
										if (ano <= 1900 || ano > Calendar.getInstance().get(Calendar.YEAR))
											JOptionPane.showMessageDialog(null, "Ano Inv�lido", "Ano Inv�lido!",JOptionPane.ERROR_MESSAGE);
										else
										{
											if (Double.parseDouble(txt_editar_valor_seguro.getValue().toString()) == 0)
												JOptionPane.showMessageDialog(null,"Valor do seguro n�o pode ser 0!", "Valor Inv�lido!",JOptionPane.ERROR_MESSAGE);
											else
											{
												int result = JOptionPane.showConfirmDialog(null, "Deseja editar o seguro da matricula '" + combo_editar_matricula_seguro.getSelectedItem()
												+ "'?", "Editar IUC", JOptionPane.YES_NO_OPTION, JOptionPane.QUESTION_MESSAGE);
												if(result == JOptionPane.YES_OPTION)
												{
													try
													{
														String id = table.getValueAt(table.getSelectedRow(), table.getColumn("IdSeguro").getModelIndex()).toString();
														String alterar = "UPDATE Seguro SET " +
																"Nome='" + txt_editar_nome_seguro.getText() +
																"',DataValidade='" + txt_editar_data_seguro.getText() +
																"',Valor=" + txt_editar_valor_seguro.getValue() +
																" WHERE IdSeguro=" + id;
														stmt.executeUpdate(alterar);
														JOptionPane.showMessageDialog(null, "Seguro editado com sucesso!", "Seguro Editado", JOptionPane.INFORMATION_MESSAGE);
														combo_editar_matricula_seguro.removeAllItems();
														txt_editar_nome_seguro.setText("");
														txt_editar_data_seguro.setText("");
														txt_editar_valor_seguro.setValue(0);
														rs = stmt.executeQuery("SELECT * FROM Seguro");
														table.setModel(Funcoes.buildTableModel(rs));
													}
													catch(SQLException sqle)
													{
														sqle.printStackTrace();
													}
												}
											}
										}
									}
								}
							}
						}
					}
				}
				else
					JOptionPane.showMessageDialog(null, "Escolha um seguro para editar!", "Seguro Inv�lido", JOptionPane.ERROR_MESSAGE);
			}
		});
		btnEditarSeugo.setFocusPainted(false);
		btnEditarSeugo.setContentAreaFilled(false);
		btnEditarSeugo.setBounds(273, 108, 116, 32);
		panel_editar_seguro.add(btnEditarSeugo);
		
		btnEliminarSeguro = new JButton("Eliminar Seguro");
		btnEliminarSeguro.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				if (table.getSelectedRow() != -1)
				{
					int result = JOptionPane.showConfirmDialog(null, "Deseja eliminar o seguro da matricula '" + combo_editar_matricula_seguro.getSelectedItem()
							+ "'?", "Eliminar Seguro", JOptionPane.YES_NO_OPTION,JOptionPane.QUESTION_MESSAGE);
					if(result == JOptionPane.YES_OPTION)
					{
						try
						{
							String id = table.getValueAt(table.getSelectedRow(), table.getColumn("IdSeguro").getModelIndex()).toString();
							String query = "DELETE FROM Seguro WHERE IdSeguro=" + id;
							stmt.executeUpdate(query);
							JOptionPane.showMessageDialog(null, "Seguro eliminado com sucesso!", "Seguro Eliminada", JOptionPane.INFORMATION_MESSAGE);
							combo_editar_matricula_seguro.removeAllItems();
							txt_editar_nome_seguro.setText("");
							txt_editar_data_seguro.setText("");
							txt_editar_valor_seguro.setValue(0);
							rs = stmt.executeQuery("SELECT * FROM IUC");
							table.setModel(Funcoes.buildTableModel(rs));
						}
						catch(SQLException sqle)
						{
							sqle.printStackTrace();
						}
					}
				}
				else
					JOptionPane.showMessageDialog(null, "Escolha um seguro para eliminar", "Seguro Inv�lido", JOptionPane.ERROR_MESSAGE);
			}
		});
		btnEliminarSeguro.setFocusPainted(false);
		btnEliminarSeguro.setContentAreaFilled(false);
		btnEliminarSeguro.setBounds(459, 109, 127, 32);
		panel_editar_seguro.add(btnEliminarSeguro);
		
		lb_editar_nome_seguro = new JLabel("<html> Nome : <font color='red'>*</font></html>");
		lb_editar_nome_seguro.setFont(new Font("Trebuchet MS", Font.PLAIN, 14));
		lb_editar_nome_seguro.setBounds(395, 42, 54, 18);
		panel_editar_seguro.add(lb_editar_nome_seguro);
		
		txt_editar_nome_seguro = new JTextField();
		txt_editar_nome_seguro.setHorizontalAlignment(SwingConstants.RIGHT);
		txt_editar_nome_seguro.setFont(new Font("Trebuchet MS", Font.PLAIN, 12));
		txt_editar_nome_seguro.setBounds(459, 41, 127, 20);
		panel_editar_seguro.add(txt_editar_nome_seguro);
		txt_editar_nome_seguro.setColumns(10);
		
		panel_total = new JPanel();
		tabbedPane.addTab("Total Taxas", null, panel_total, null);
		panel_total.setLayout(null);
		
		lb_matricula_total = new JLabel("<html> Matr\u00EDcula : <font color='red'>*</font></html>");
		lb_matricula_total.setFont(new Font("Trebuchet MS", Font.PLAIN, 14));
		lb_matricula_total.setBounds(127, 10, 76, 18);
		panel_total.add(lb_matricula_total);
		
		combo_total_matricula = new JComboBox<String>();
		combo_total_matricula.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				txt_total_inspecao.setText("0");
				txt_total_iuc.setText("0");
				txt_total_revisao.setText("0");
				txt_total_seguro.setText("0");
			}
		});
		combo_total_matricula.setBounds(213, 11, 116, 20);
		panel_total.add(combo_total_matricula);
		
		lblTotalDasInspees = new JLabel("Total das Inspe\u00E7\u00F5es :");
		lblTotalDasInspees.setFont(new Font("Trebuchet MS", Font.PLAIN, 14));
		lblTotalDasInspees.setBounds(70, 60, 133, 18);
		panel_total.add(lblTotalDasInspees);
		
		lblTotalDosIucs = new JLabel("Total dos IUCs :");
		lblTotalDosIucs.setFont(new Font("Trebuchet MS", Font.PLAIN, 14));
		lblTotalDosIucs.setBounds(105, 110, 98, 18);
		panel_total.add(lblTotalDosIucs);
		
		lblTotalDasRevises = new JLabel("Total das Revis\u00F5es :");
		lblTotalDasRevises.setFont(new Font("Trebuchet MS", Font.PLAIN, 14));
		lblTotalDasRevises.setBounds(78, 160, 125, 18);
		panel_total.add(lblTotalDasRevises);
		
		lblTotalDoSeguro = new JLabel("Total do Seguro :");
		lblTotalDoSeguro.setFont(new Font("Trebuchet MS", Font.PLAIN, 14));
		lblTotalDoSeguro.setBounds(95, 210, 108, 18);
		panel_total.add(lblTotalDoSeguro);
		
		txt_total_inspecao = new JTextField();
		txt_total_inspecao.setBounds(213, 60, 116, 20);
		panel_total.add(txt_total_inspecao);
		txt_total_inspecao.setColumns(10);
		
		txt_total_iuc = new JTextField();
		txt_total_iuc.setColumns(10);
		txt_total_iuc.setBounds(213, 110, 116, 20);
		panel_total.add(txt_total_iuc);
		
		txt_total_revisao = new JTextField();
		txt_total_revisao.setColumns(10);
		txt_total_revisao.setBounds(213, 160, 116, 20);
		panel_total.add(txt_total_revisao);
		
		txt_total_seguro = new JTextField();
		txt_total_seguro.setColumns(10);
		txt_total_seguro.setBounds(213, 210, 116, 20);
		panel_total.add(txt_total_seguro);
		
		btnCalcularTotal = new JButton("Calcular Total");
		btnCalcularTotal.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				String matricula = combo_total_matricula.getSelectedItem().toString();
				try
				{
					rs = stmt.executeQuery("SELECT SUM(Valor),Matricula FROM Inspecao WHERE Matricula LIKE '" + matricula + "'");
					if(rs.next())
						txt_total_inspecao.setText(rs.getString(1));
					if(rs.getString(1) == null)
						txt_total_inspecao.setText("0");
					rs = stmt.executeQuery("SELECT SUM(Valor),Matricula FROM IUC WHERE Matricula LIKE '" + matricula + "'");
					if(rs.next())
						txt_total_iuc.setText(rs.getString(1));
					if(rs.getString(1) == null)
						txt_total_iuc.setText("0");
					rs = stmt.executeQuery("SELECT SUM(Valor),Matricula FROM Revisao WHERE Matricula LIKE '" + matricula + "'");
					if(rs.next())
						txt_total_revisao.setText(rs.getString(1));
					if(rs.getString(1) == null)
						txt_total_revisao.setText("0");
					rs = stmt.executeQuery("SELECT SUM(Valor),Matricula FROM Seguro WHERE Matricula LIKE '" + matricula + "'");
					if(rs.next())
						txt_total_seguro.setText(rs.getString(1));
					if(rs.getString(1) == null)
						txt_total_seguro.setText("0");
				}
				catch ( Exception ex )
				{
					ex.printStackTrace();
				}
			}
		});
		btnCalcularTotal.setFocusPainted(false);
		btnCalcularTotal.setContentAreaFilled(false);
		btnCalcularTotal.setBounds(213, 252, 116, 32);
		panel_total.add(btnCalcularTotal);
	}

	public void preencherCB(JComboBox<String> cb) {
		try {
			rs = stmt.executeQuery("SELECT Matricula FROM Carro");
			while (rs.next())
				cb.addItem(rs.getString(1));
		} catch (SQLException se) {
			se.printStackTrace();
		}
	}
	
	public void modificarSpinners(JSpinner js)
	{
		JSpinner.NumberEditor jsEditor = (JSpinner.NumberEditor) js.getEditor();
		DefaultFormatter formatter = (DefaultFormatter) jsEditor.getTextField().getFormatter();
		formatter.setAllowsInvalid(false);
	}
}
