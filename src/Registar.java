import java.awt.*;
import javax.swing.*;
import java.awt.event.*;
import java.sql.*;

public class Registar extends JFrame {

	public static final String JDBC_DRIVER = "org.sqlite.JDBC";
	public static final String DB_URL = "jdbc:sqlite:projeto.db";
	public Connection conn = null;
	public Statement stmt = null;
	public ResultSet rs;
	
	private JPanel contentPane;
	private JPasswordField txt_pass;
	private JPasswordField txt_pass_confirm;
	private JTextField txt_user;

	
	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					Registar frame = new Registar();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public Registar() {
		setTitle("Registar Utilizador");
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setIconImage(new ImageIcon("add user.png").getImage());
		setBounds(100, 100, 298, 210);
		contentPane = new JPanel();
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		try{
			Class.forName(JDBC_DRIVER);
			conn = DriverManager.getConnection(DB_URL);
			stmt = conn.createStatement();
		}catch(ClassNotFoundException e){
			e.printStackTrace();
		}catch(SQLException se){
			se.printStackTrace();
		}
		
		JLabel lblUtilizador = new JLabel("Utilizador :");
		lblUtilizador.setFont(new Font("Trebuchet MS", Font.PLAIN, 14));
		lblUtilizador.setBounds(42, 13, 70, 14);
		contentPane.add(lblUtilizador);
		
		JLabel lblPassword = new JLabel("Password :");
		lblPassword.setFont(new Font("Trebuchet MS", Font.PLAIN, 14));
		lblPassword.setBounds(44, 38, 68, 14);
		contentPane.add(lblPassword);
		
		JLabel lblConfirmarPassword = new JLabel("Confirmar Pass :");
		lblConfirmarPassword.setFont(new Font("Trebuchet MS", Font.PLAIN, 14));
		lblConfirmarPassword.setBounds(10, 63, 108, 14);
		contentPane.add(lblConfirmarPassword);
		
		JButton btnRegistar = new JButton("Registar");
		btnRegistar.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				JLabel msgLabel = new JLabel("Caixas Vazias!", JLabel.CENTER);
				String pass = new String(txt_pass.getPassword()).trim(), pass_confirm = new String(txt_pass_confirm.getPassword()).trim();
				if(txt_user.getText().isEmpty() || pass.isEmpty() || pass_confirm.isEmpty())
					JOptionPane.showMessageDialog(null, msgLabel, "Caixas Vazias", JOptionPane.ERROR_MESSAGE);
				else
				{
					try
					{
						rs = stmt.executeQuery("SELECT User FROM Users WHERE User LIKE '" + txt_user.getText() + "'");
						if( rs.isBeforeFirst() )
							JOptionPane.showMessageDialog(null, "User '" + txt_user.getText() + "'est� a ser usado!", "User Inv�lido", JOptionPane.INFORMATION_MESSAGE);
						else
						{
							if( !pass.equals(pass_confirm))
								JOptionPane.showMessageDialog(null, "As senhas inseridas n�o s�o iguais", "Senhas diferentes!", JOptionPane.INFORMATION_MESSAGE);
							else
							{
								String insert = "INSERT INTO Users(User,Pass)VALUES('" + txt_user.getText() + "','" + pass + "')";
								stmt.executeUpdate(insert);
								JOptionPane.showMessageDialog(null, "User adicionado com sucesso!", "User Registado", JOptionPane.INFORMATION_MESSAGE);
								dispose();
							}
						}
					}catch(SQLException se){
						se.printStackTrace();
					}
				}
			}
		});
		btnRegistar.setBounds(128, 90, 147, 23);
		btnRegistar.setFocusPainted(false);
		btnRegistar.setContentAreaFilled(false);
		contentPane.add(btnRegistar);
		
		txt_user = new JTextField();
		txt_user.setColumns(10);
		txt_user.setBounds(128, 11, 147, 20);
		contentPane.add(txt_user);
		
		txt_pass = new JPasswordField();
		txt_pass.setColumns(10);
		txt_pass.setBounds(128, 36, 147, 20);
		contentPane.add(txt_pass);
		
		txt_pass_confirm = new JPasswordField();
		txt_pass_confirm.setColumns(10);
		txt_pass_confirm.setBounds(128, 61, 147, 20);
		contentPane.add(txt_pass_confirm);
		
		JButton btnCancelar = new JButton("Cancelar");
		btnCancelar.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent arg0) {
				dispose();
			}
		});
		btnCancelar.setFocusPainted(false);
		btnCancelar.setContentAreaFilled(false);
		btnCancelar.setBounds(128, 124, 147, 23);
		contentPane.add(btnCancelar);
		
		JLabel label_imagem = new JLabel("");
		label_imagem.setIcon(new ImageIcon("add user.png"));
		label_imagem.setFont(new Font("Trebuchet MS", Font.PLAIN, 14));
		label_imagem.setBounds(43, 93, 64, 64);
		contentPane.add(label_imagem);
		
		Dimension dimension = Toolkit.getDefaultToolkit().getScreenSize();
	    int x = (int) ((dimension.getWidth() - getWidth()) / 2);
	    int y = (int) ((dimension.getHeight() - getHeight()) / 2);
		setLocation(x,y);
		setResizable(false);
	}
}
