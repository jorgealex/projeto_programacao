import java.awt.*;
import javax.swing.*;
import javax.swing.border.*;
import javax.swing.text.*;
import java.util.*;
import java.awt.event.*;
import java.sql.*;

public class InserirCarro extends JFrame {

	private JPanel contentPane;
	private JTextField txt_marca;
	private JTextField txt_modelo;
	private JTextField txt_cor;
	private JSpinner txt_cilindrada;
	private JFormattedTextField txt_matricula;
	private JLabel lblAno;
	Funcoes f;
	public static final String JDBC_DRIVER = "org.sqlite.JDBC";
	public static final String DB_URL = "jdbc:sqlite:projeto.db";
	public static Connection conn = null;
	public static Statement stmt = null;
	public static ResultSet rs;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					InserirCarro frame = new InserirCarro();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	
	/**
	 * Create the frame.
	 */
	public InserirCarro() {
		try {
			Class.forName(JDBC_DRIVER);
			conn = DriverManager.getConnection(DB_URL);
			stmt = conn.createStatement();
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
		} catch (SQLException se) {
			se.printStackTrace();
		}

		setTitle("Inserir Carro");
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 310, 396);
		setIconImage(new ImageIcon("add.png").getImage());
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		f = new Funcoes();
		
		JLabel lblMarca = new JLabel("<html>Marca : <font color='red'>*</font></html>");
		lblMarca.setBounds(15, 38, 55, 18);
		lblMarca.setFont(new Font("Trebuchet MS", Font.PLAIN, 14));
		
		txt_marca = new JTextField();
		txt_marca.setBounds(91, 37, 177, 21);
		txt_marca.setFont(new Font("Trebuchet MS", Font.PLAIN, 12));
		txt_marca.setColumns(10);

		JLabel lblModelo = new JLabel("<html>Modelo : <font color='red'>*</font></html>");
		lblModelo.setBounds(15, 63, 64, 18);
		lblModelo.setFont(new Font("Trebuchet MS", Font.PLAIN, 14));

		txt_modelo = new JTextField();
		txt_modelo.setBounds(91, 62, 177, 21);
		txt_modelo.setFont(new Font("Trebuchet MS", Font.PLAIN, 12));
		txt_modelo.setColumns(10);

		JLabel lblCor = new JLabel("<html>Cor : <font color='red'>*</font></html>");
		lblCor.setBounds(15, 88, 39, 18);
		lblCor.setFont(new Font("Trebuchet MS", Font.PLAIN, 14));

		txt_cor = new JTextField();
		txt_cor.setBounds(91, 87, 177, 21);
		txt_cor.setFont(new Font("Trebuchet MS", Font.PLAIN, 12));
		txt_cor.setColumns(10);

		JLabel lblCilindrada = new JLabel("<html>Cilindrada : <font color='red'>*</font></html>");
		lblCilindrada.setBounds(15, 113, 80, 18);
		lblCilindrada.setFont(new Font("Trebuchet MS", Font.PLAIN, 14));

		txt_cilindrada = new JSpinner(new SpinnerNumberModel(500, 500, 2000, 100));
		txt_cilindrada.setBounds(111, 112, 157, 22);
		txt_cilindrada.setFont(new Font("Trebuchet MS", Font.PLAIN, 12));
		txt_cilindrada.setEditor(new JSpinner.NumberEditor(txt_cilindrada, "0000"));
		JSpinner.NumberEditor jsEditor = (JSpinner.NumberEditor) txt_cilindrada.getEditor();
		DefaultFormatter formatter = (DefaultFormatter) jsEditor.getTextField().getFormatter();
		formatter.setAllowsInvalid(false);

		JLabel lblMatricula = new JLabel("<html> Matr�cula : <font color='red'>*</font></html>");
		lblMatricula.setBounds(15, 141, 76, 18);
		lblMatricula.setFont(new Font("Trebuchet MS", Font.PLAIN, 14));

		txt_matricula = new JFormattedTextField();
		txt_matricula.addKeyListener(new KeyAdapter() {
			@Override
			public void keyTyped(KeyEvent arg0) {
				if(Character.isLowerCase(arg0.getKeyChar()))
					arg0.setKeyChar(Character.toUpperCase(arg0.getKeyChar()));
			}
		});
		txt_matricula.setHorizontalAlignment(SwingConstants.RIGHT);
		txt_matricula.setBounds(111, 140, 157, 21);
		txt_matricula.setFont(new Font("<html> Trebuchet MS", Font.PLAIN, 12));
		txt_matricula.setColumns(10);
		Funcoes.adicionarMask("AA-AA-AA", txt_matricula);

		JLabel lblCombustivel = new JLabel("<html> Combust�vel : <font color='red'>*</font></html>");
		lblCombustivel.setBounds(15, 172, 95, 18);
		lblCombustivel.setFont(new Font("Trebuchet MS", Font.PLAIN, 14));

		lblAno = new JLabel("<html> Ano de Aquisi��o : <font color='red'>*</font></html>");
		lblAno.setBounds(15, 201, 128, 18);
		lblAno.setFont(new Font("Trebuchet MS", Font.PLAIN, 14));

		JComboBox<String> txt_combustivel = new JComboBox<String>();
		txt_combustivel.setBounds(111, 172, 157, 20);
		try {
			rs = stmt.executeQuery("SELECT * FROM Combustiveis");
			while (rs.next())
				txt_combustivel.addItem(rs.getString(1));
		} catch (SQLException se) {
			se.printStackTrace();
		}

		JFormattedTextField txt_ano = new JFormattedTextField();
		txt_ano.setHorizontalAlignment(SwingConstants.RIGHT);
		txt_ano.setBounds(162, 200, 106, 22);
		txt_ano.setFont(new Font("Trebuchet MS", Font.PLAIN, 12));
		Funcoes.adicionarMask("####", txt_ano);

		JLabel lblPreoDeAquisio = new JLabel("<html> Pre�o de Aquisi��o : <font color='red'>*</font></html>");
		lblPreoDeAquisio.setBounds(15, 230, 139, 18);
		lblPreoDeAquisio.setFont(new Font("Trebuchet MS", Font.PLAIN, 14));

		JSpinner txt_preco_aq = new JSpinner(new SpinnerNumberModel(0, 0, 10000000, 100));
		txt_preco_aq.setBounds(162, 229, 106, 22);
		txt_preco_aq.setFont(new Font("Trebuchet MS", Font.PLAIN, 12));
		JSpinner.NumberEditor jsEditor3 = (JSpinner.NumberEditor) txt_preco_aq.getEditor();
		DefaultFormatter formatter3 = (DefaultFormatter) jsEditor3.getTextField().getFormatter();
		formatter3.setAllowsInvalid(false);

		contentPane.setLayout(null);
		contentPane.add(lblPreoDeAquisio);
		contentPane.add(txt_preco_aq);
		contentPane.add(lblMarca);
		contentPane.add(txt_marca);
		contentPane.add(lblModelo);
		contentPane.add(txt_modelo);
		contentPane.add(lblCor);
		contentPane.add(txt_cor);
		contentPane.add(lblCilindrada);
		contentPane.add(txt_cilindrada);
		contentPane.add(lblMatricula);
		contentPane.add(txt_matricula);
		contentPane.add(lblCombustivel);
		contentPane.add(txt_combustivel);
		contentPane.add(lblAno);
		contentPane.add(txt_ano);

		JLabel lblAnoVenda = new JLabel("Ano Venda :");
		lblAnoVenda.setFont(new Font("Trebuchet MS", Font.PLAIN, 14));
		lblAnoVenda.setBounds(15, 260, 78, 18);
		contentPane.add(lblAnoVenda);

		JFormattedTextField txt_ano_venda = new JFormattedTextField();
		txt_ano_venda.setHorizontalAlignment(SwingConstants.RIGHT);
		txt_ano_venda.setFont(new Font("Trebuchet MS", Font.PLAIN, 12));
		txt_ano_venda.setBounds(111, 259, 157, 22);
		Funcoes.adicionarMask("####", txt_ano_venda);
		contentPane.add(txt_ano_venda);

		JLabel lblValorVenda = new JLabel("Valor Venda :");
		lblValorVenda.setFont(new Font("Trebuchet MS", Font.PLAIN, 14));
		lblValorVenda.setBounds(15, 289, 85, 18);
		contentPane.add(lblValorVenda);

		
		JSpinner txt_valor_venda = new JSpinner(new SpinnerNumberModel(0.0, 0.0, 10000000.0, 100.0));
		txt_valor_venda.setFont(new Font("Trebuchet MS", Font.PLAIN, 12));
		txt_valor_venda.setBounds(111, 289, 157, 22);
		JSpinner.NumberEditor jsEditor5 = (JSpinner.NumberEditor) txt_valor_venda.getEditor();
		DefaultFormatter formatter5 = (DefaultFormatter) jsEditor5.getTextField().getFormatter();
		formatter5.setAllowsInvalid(false);
		contentPane.add(txt_valor_venda);

		JButton btnInserir = new JButton("Inserir");
		btnInserir.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent arg0)
			{
				String ano_venda = txt_ano_venda.getText().trim();//substituir espa�os por nada...
				String valor_venda = txt_valor_venda.getValue().toString().trim();
				
				if ( ano_venda.isEmpty() )
					ano_venda = "0";
				
				if ( valor_venda.isEmpty() )
					valor_venda = "0";
				
				if (txt_marca.getText().isEmpty()//verificar se algum campo obrigat�rio est� vazio
					|| txt_modelo.getText().isEmpty()
					|| txt_cor.getText().isEmpty()
					|| txt_ano.getText().trim().isEmpty()
					|| Integer.parseInt(txt_preco_aq.getValue().toString()) == 0)
					JOptionPane.showMessageDialog(null, "Insira dados nos campos obrigat�rios", "Caixas de texto vazias", JOptionPane.INFORMATION_MESSAGE);
				else
				{
					if (Integer.parseInt(txt_ano.getText().trim()) > Integer.parseInt(ano_venda) && Integer.parseInt(ano_venda) != 0)//verificar se o ano de aquisi��o � maior que o de venda
						JOptionPane.showMessageDialog(null, "Ano de aquisi��o n�o pode ser maior do	 que o ano de venda", "Ano Inv�lido",JOptionPane.INFORMATION_MESSAGE);
					else
					{
						if(txt_matricula.getText().trim().length() != 8)
							JOptionPane.showMessageDialog(null, "Matricula Incompleta", "Matricula Incompleta",JOptionPane.INFORMATION_MESSAGE);
						else
						{
							if(txt_ano.getText().trim().length() != 4)
								JOptionPane.showMessageDialog(null, "Ano de aquisi��o � composto por quatro valores", "Ano de Aquisi��o Inv�lido",JOptionPane.INFORMATION_MESSAGE);
							else
							{
								try
								{
									rs = stmt.executeQuery("SELECT Matricula FROM Carro WHERE Matricula LIKE '" + txt_matricula.getText() + "'");
		
									if (rs.isBeforeFirst())
										JOptionPane.showMessageDialog(null, "Matricula est� a ser usada", "Matricula inv�lida", JOptionPane.INFORMATION_MESSAGE);
									else
									{
										if (Integer.parseInt(txt_ano.getText().trim()) > Calendar.getInstance().get(Calendar.YEAR) || Integer.parseInt(ano_venda) > Calendar.getInstance().get(Calendar.YEAR))
											JOptionPane.showMessageDialog(null, "Um dos anos inseridos � maior do que o ano atual", "Ano Inv�lido", JOptionPane.INFORMATION_MESSAGE);
										else
										{
											if ( ano_venda.length() != 4 && Integer.parseInt(ano_venda) != 0)
												JOptionPane.showMessageDialog(null, "Ano de venda necessita de quatro numeros inteiros!", "Ano Inv�lido",JOptionPane.INFORMATION_MESSAGE);
											else
											{
												if(Integer.parseInt(ano_venda) != 0 && Double.parseDouble(valor_venda) == 0)
													JOptionPane.showMessageDialog(null, "Insira o valor de venda", "Valor de Venda Vazio", JOptionPane.INFORMATION_MESSAGE);
												else
												{
													String insert = "INSERT INTO Carro(Marca,Modelo,Cor,Cilindrada,Matricula,Combustivel,AnoAquisicao,PrecoAquisicao,AnoVenda,ValorVenda)"
															+ "VALUES('" + txt_marca.getText() + "','" + txt_modelo.getText() + "','"
															+ txt_cor.getText() + "'," + txt_cilindrada.getValue().toString() + ",'"
															+ txt_matricula.getText() + "','" + txt_combustivel.getSelectedItem().toString() + "'," + txt_ano.getText()
															+ "," + txt_preco_aq.getValue().toString() + "," + ano_venda + "," + valor_venda + ")";
													stmt.executeUpdate(insert);
													JOptionPane.showMessageDialog(null, "Carro Inserido Com Sucesso!", "Carro Inserido", JOptionPane.INFORMATION_MESSAGE);
													dispose();
												}
											}
										}
									}
								} catch (SQLException se) {
									System.out.println(se.getMessage());
								}
							}
						}
					}
				}
			}
		});
		btnInserir.setBounds(198, 322, 71, 22);
		btnInserir.setFocusPainted(false);
		btnInserir.setContentAreaFilled(false);
		contentPane.add(btnInserir);

		JLabel label = new JLabel("");
		label.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent arg0) {
				String resp = JOptionPane.showInputDialog(null, "Insira o novo nome do combustivel:", "Novo Combustivel", JOptionPane.PLAIN_MESSAGE);
				try
				{
					if(resp != null)
					{
						rs.close();
						rs = stmt.executeQuery("SELECT UPPER(NomeCombustivel) FROM Combustiveis WHERE NomeCombustivel LIKE '" + resp.toUpperCase() + "'");
						if(resp != null && !resp.isEmpty() && !rs.isBeforeFirst())
						{
							rs.close();
							stmt.close();
							stmt.executeUpdate("INSERT INTO Combustiveis(NomeCombustivel)VALUES('" + resp + "')");
							txt_combustivel.removeAllItems();
							rs.close();
							rs = stmt.executeQuery("SELECT NomeCombustivel FROM Combustiveis");
							while (rs.next())
								txt_combustivel.addItem(rs.getString(1));
						}
						else
							JOptionPane.showMessageDialog(null, "Combustivel Inv�lido ou j� est� inserido na base de dados!", "Combustivel Inv�lido", JOptionPane.INFORMATION_MESSAGE);
					}
				}catch(SQLException se){
					se.printStackTrace();
				}
			}
		});
		label.setIcon(new ImageIcon("plus.png"));
		label.setBounds(271, 174, 16, 16);
		contentPane.add(label);
		
		JLabel label_1 = new JLabel("<html><font color='red'>* Obrigat�rio</font></html>");
		label_1.setFont(new Font("Trebuchet MS", Font.PLAIN, 12));
		label_1.setBounds(15, 11, 71, 15);
		contentPane.add(label_1);

		Dimension dimension = Toolkit.getDefaultToolkit().getScreenSize();
		int x = (int) ((dimension.getWidth() - this.getWidth()) / 2);
		int y = (int) ((dimension.getHeight() - this.getHeight()) / 2);
		setLocation(x, y);
		setResizable(false);
	}
}
