import java.sql.*;
import java.util.*;
import javax.swing.*;
import javax.swing.table.DefaultTableModel;
import javax.swing.text.MaskFormatter;

/**
 * @author Jorge
 *
 */
public class Funcoes {
	public static DefaultTableModel buildTableModel(ResultSet rs) throws SQLException
	{

	    ResultSetMetaData metaData = rs.getMetaData();

	    // names of columns
	    Vector<String> columnNames = new Vector<String>();
	    int columnCount = metaData.getColumnCount();
	    
	    for (int column = 1; column <= columnCount; column++)
	        columnNames.add(metaData.getColumnName(column));
	    

	    // data of the table
	    Vector<Vector<Object>> data = new Vector<Vector<Object>>();
	    while (rs.next())
	    {
	        Vector<Object> vector = new Vector<Object>();
	        for (int columnIndex = 1; columnIndex <= columnCount; columnIndex++)
	            vector.add(rs.getObject(columnIndex));
	        data.add(vector);
	    }
	    
	    return new DefaultTableModel(data, columnNames);

	}
	public static void adicionarMask(String formato, JFormattedTextField field) {
		try {
			MaskFormatter mask = new MaskFormatter(formato);
			mask.install(field);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
}
