import java.awt.*;
import java.sql.*;

import javax.swing.*;

import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;

public class ConsultarCarros extends JFrame  {
	
	public static final String JDBC_DRIVER = "org.sqlite.JDBC";
	public static final String DB_URL = "jdbc:sqlite:projeto.db";
	public static Connection conn = null;
	public static Statement stmt = null;
	public ResultSet rs;
	Funcoes f = new Funcoes();
	
	private JPanel contentPane;
	private JTable table;
	private JButton btnEditarRegisto;
	private JButton btnEliminarRegisto;
	private JButton btnAtualizarTabela;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					ConsultarCarros frame = new ConsultarCarros();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}
	
	/**
	 * Create the frame.
	 */
	public ConsultarCarros() throws Exception
	{
		setTitle("Consultar Carros");
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 870, 360);
		contentPane = new JPanel();
		setContentPane(contentPane);
		contentPane.setLayout(null);
		setIconImage(new ImageIcon("search.png").getImage());
		Class.forName(JDBC_DRIVER);
		conn = DriverManager.getConnection(DB_URL);
		stmt = conn.createStatement();
		rs = stmt.executeQuery("SELECT * FROM Carro");
		table = new JTable(Funcoes.buildTableModel(rs)){
			public boolean isCellEditable(int rowIndex, int mColIndex)
			{
				return false;
			}
		};
		table.addMouseListener(new MouseAdapter() {
			@Override
			public void mousePressed(MouseEvent me) {
				JTable table =(JTable) me.getSource();
		        Point p = me.getPoint();
		        int row = table.rowAtPoint(p);
		        if (me.getClickCount() == 2 && row != -1)
		        {
		        	EditarCarro ec;
					try {
						ec = new EditarCarro();
						ec.setLbl_matricula("" + table.getModel().getValueAt(table.getSelectedRow(), 4));
						ec.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
						ec.setVisible(true);
					} catch (Exception e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
		        }
			}
		});
		table.setBounds(0, 43, 614, 236);
		table.getTableHeader().setReorderingAllowed(false);
		JScrollPane scrollPane = new JScrollPane(table);
		scrollPane.setBounds(0, 0, 864, 287);
		contentPane.add(scrollPane);
		
		btnEditarRegisto = new JButton("Editar Registo");
		btnEditarRegisto.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				if(table.getSelectedRow() == -1)
					JOptionPane.showMessageDialog(null, "Seleccione um carro para editar!","Editar Carro", JOptionPane.INFORMATION_MESSAGE);
				else
				{
					EditarCarro ec;
					try {
						ec = new EditarCarro();
						ec.setLbl_matricula("" + table.getModel().getValueAt(table.getSelectedRow(), 4));
						ec.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
						ec.setVisible(true);
					} catch (Exception e1) {
						// TODO Auto-generated catch block
						e1.printStackTrace();
					}
					
				}
			}
		});
		btnEditarRegisto.setFocusPainted(false);
		btnEditarRegisto.setContentAreaFilled(false);
		btnEditarRegisto.setBounds(583, 298, 126, 23);
		contentPane.add(btnEditarRegisto);
		
		btnEliminarRegisto = new JButton("Eliminar Registo");
		btnEliminarRegisto.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				if(table.getSelectedRow() == -1)
					JOptionPane.showMessageDialog(null, "Seleccione um carro para eliminar!","Eliminar Carro", JOptionPane.INFORMATION_MESSAGE);
				else
				{
					try
					{
						String matricula = table.getModel().getValueAt(table.getSelectedRow(), 4).toString();
						int result = JOptionPane.showConfirmDialog(null, "Deseja eliminar o carro com a matricula '" + matricula + "'?", "Eliminar Carro", JOptionPane.YES_NO_OPTION,JOptionPane.QUESTION_MESSAGE);
						if(result == JOptionPane.YES_OPTION)
						{
							String query = "DELETE FROM Carro WHERE Matricula LIKE'" + table.getModel().getValueAt(table.getSelectedRow(), 4).toString() + "'";
							stmt.executeUpdate(query);
							AtualizarTabela();
						}
					}
					catch(SQLException e1)
					{
						e1.printStackTrace();
					}
					
				}
			}
		});
		btnEliminarRegisto.setFocusPainted(false);
		btnEliminarRegisto.setContentAreaFilled(false);
		btnEliminarRegisto.setBounds(728, 298, 126, 23);
		contentPane.add(btnEliminarRegisto);
		
		btnAtualizarTabela = new JButton("Atualizar Tabela");
		btnAtualizarTabela.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				AtualizarTabela();
			}
		});
		btnAtualizarTabela.setFocusPainted(false);
		btnAtualizarTabela.setContentAreaFilled(false);
		btnAtualizarTabela.setBounds(435, 298, 138, 23);
		contentPane.add(btnAtualizarTabela);
		
		Dimension dimension = Toolkit.getDefaultToolkit().getScreenSize();
		int x = (int) ((dimension.getWidth() - this.getWidth()) / 2);
		int y = (int) ((dimension.getHeight() - this.getHeight()) / 2);
		setLocation(x, y);
		setResizable(false);
		
	}
	
	public void AtualizarTabela()
	{
		try {
			rs = stmt.executeQuery("SELECT * FROM Carro");
			table.setModel(Funcoes.buildTableModel(rs));
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}

	/**
	 * @param btnEditarRegisto the btnEditarRegisto to set
	 */
	public void setBtnEditarRegisto(boolean v) {
		this.btnEditarRegisto.setVisible(v);
	}

	/**
	 * @param btnEliminarRegisto the btnEliminarRegisto to set
	 */
	public void setBtnEliminarRegisto(boolean v) {
		this.btnEliminarRegisto.setVisible(v);
	}

	/**
	 * @param btnAtualizarTabela the btnAtualizarTabela to set
	 */
	public void setBtnAtualizarTabela(boolean v) {
		this.btnAtualizarTabela.setVisible(v);
	}
}
