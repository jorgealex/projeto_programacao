import java.awt.*;

import javax.imageio.ImageIO;
import javax.swing.*;
import java.sql.*;
import java.awt.event.*;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;

public class projeto {

	public static final String JDBC_DRIVER = "org.sqlite.JDBC";
	public static final String DB_URL = "jdbc:sqlite:projeto.db";
	public Connection conn = null;
	public Statement stmt = null;
	public ResultSet rs;
	public JLabel centrar = new JLabel("",JLabel.CENTER);
	private JFrame frmBaseDeDados;
	private JTextField txt_user;
	private JPasswordField txt_pass;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					projeto window = new projeto();
					window.frmBaseDeDados.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the application.
	 */
	public projeto() {
		initialize();
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize()
	{
		try{
			Class.forName(JDBC_DRIVER);
			conn = DriverManager.getConnection(DB_URL);
			stmt = conn.createStatement();
		}catch(ClassNotFoundException e){
			e.printStackTrace();
		}catch(SQLException se){
			se.printStackTrace();
		}
		
		frmBaseDeDados = new JFrame();
		frmBaseDeDados.setTitle("Base de Dados Automovel");
		frmBaseDeDados.setBounds(100, 100, 354, 372);
		frmBaseDeDados.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frmBaseDeDados.getContentPane().setLayout(null);
		frmBaseDeDados.setIconImage(new ImageIcon("carro.png").getImage());
		
		JLabel lblUtilizador = new JLabel("Utilizador:");
		lblUtilizador.setFont(new Font("Trebuchet MS", Font.BOLD, 22));
		lblUtilizador.setBounds(26, 203, 110, 29);
		frmBaseDeDados.getContentPane().add(lblUtilizador);
	
		JLabel lblPassword = new JLabel("Password:");
		lblPassword.setFont(new Font("Trebuchet MS", Font.BOLD, 22));
		lblPassword.setBounds(33, 243, 103, 29);
		frmBaseDeDados.getContentPane().add(lblPassword);
		
		JButton btnLogIn = new JButton("Log In");
		btnLogIn.setFocusPainted(false);
		btnLogIn.setContentAreaFilled(false);
		btnLogIn.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				centrar.setText("O Utilizador ou a password est�o vazios!");
				String pass = new String(txt_pass.getPassword());
				if( txt_user.getText().isEmpty() || pass.isEmpty())
					JOptionPane.showMessageDialog(null, centrar, "N�o foi encontrado o utilizador", JOptionPane.ERROR_MESSAGE);
				else
				{
					try{
						rs = stmt.executeQuery("SELECT User FROM Users WHERE User LIKE'" + txt_user.getText() + "'");
						centrar.setText("Utilizador " + txt_user.getText() + " n�o foi encontrado na base de dados!");
						if( !rs.isBeforeFirst() )
							JOptionPane.showMessageDialog(null, centrar, "N�o foi encontrado o utilizador", JOptionPane.ERROR_MESSAGE);
						else
						{
							rs = stmt.executeQuery("SELECT User, Pass FROM Users WHERE User LIKE'" + txt_user.getText() + "' AND Pass LIKE'" + pass + "'");
							centrar.setText("Password est� incorrecta");
							if( !rs.isBeforeFirst() )
								JOptionPane.showMessageDialog(null, centrar, "Utilizador", JOptionPane.ERROR_MESSAGE);
							else
							{
								MainForm form = new MainForm();
								form.setVisible(true);
								frmBaseDeDados.dispose();
							}
						}
					}catch(SQLException sqle){
						sqle.printStackTrace();
					}
				}
			}
		});
		btnLogIn.setBounds(147, 277, 89, 22);
		frmBaseDeDados.getContentPane().add(btnLogIn);
		
		txt_user = new JTextField();
		txt_user.setFont(new Font("Trebuchet MS", Font.PLAIN, 12));
		txt_user.setBounds(147, 206, 184, 20);
		frmBaseDeDados.getContentPane().add(txt_user);
		txt_user.setColumns(10);
		
		txt_pass = new JPasswordField();
		txt_pass.setFont(new Font("Trebuchet MS", Font.PLAIN, 12));
		txt_pass.setColumns(10);
		txt_pass.setBounds(147, 246, 184, 20);
		frmBaseDeDados.getContentPane().add(txt_pass);
		
		JButton btnRegistar = new JButton("Registar");
		btnRegistar.setFocusPainted(false);
		btnRegistar.setContentAreaFilled(false);
		btnRegistar.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent arg0) {
				Registar r = new Registar();
				r.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
				r.setVisible(true);
			}
		});
		btnRegistar.setBounds(242, 277, 89, 22);
		frmBaseDeDados.getContentPane().add(btnRegistar);
		
		JButton btnAnonimo = new JButton("Anonimo");
		btnAnonimo.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				try {
					ConsultarCarros cc = new ConsultarCarros();
					cc.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
					cc.setVisible(true);
					cc.setBtnAtualizarTabela(false);
					cc.setBtnEditarRegisto(false);
					cc.setBtnEliminarRegisto(false);
					frmBaseDeDados.dispose();
				} catch (Exception e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
			}
		});
		btnAnonimo.setFocusPainted(false);
		btnAnonimo.setContentAreaFilled(false);
		btnAnonimo.setBounds(147, 310, 89, 22);
		frmBaseDeDados.getContentPane().add(btnAnonimo);
		
		JLabel imagem = new JLabel("");
		try {
			BufferedImage pic = ImageIO.read(new File("carro.png"));
			imagem.setIcon(new ImageIcon(pic));
		} catch (IOException e1) {
			e1.printStackTrace();
		}
		
		imagem.setFont(new Font("Trebuchet MS", Font.BOLD, 24));
		imagem.setBounds(159, 28, 128, 128);
		frmBaseDeDados.getContentPane().add(imagem);
		frmBaseDeDados.getContentPane().setFocusTraversalPolicy(new FocusTraversalOnArray(new Component[]{txt_user, txt_pass, btnLogIn, btnRegistar, btnAnonimo}));
		
		Dimension dimension = Toolkit.getDefaultToolkit().getScreenSize();
	    int x = (int) ((dimension.getWidth() - frmBaseDeDados.getWidth()) / 2);
	    int y = (int) ((dimension.getHeight() - frmBaseDeDados.getHeight()) / 2);
		frmBaseDeDados.setLocation(x,y);
		
		frmBaseDeDados.setResizable(false);
		frmBaseDeDados.setFocusTraversalPolicy(new FocusTraversalOnArray(new Component[]{txt_user, txt_pass, btnLogIn, btnRegistar, btnAnonimo}));
		
	}
}
